//
// Created by lizhe on 12/29/2016.
//

#ifndef SNAKER_RESOURCE_HANDLER_HPP
#define SNAKER_RESOURCE_HANDLER_HPP

#include <windows.h>
template<typename T>
class handler
{
public:
    handler(T t)
        : handled(t)
    {}
    handler(handler const&) = delete;
    handler(handler&& src) noexcept
        : handled(src.handled)
    {
        src.handled = NULL;
    }
    ~handler()
    {
        DeleteObject(handled);
    }
    T handled;
};

template<typename T>
handler<T> handle(T t)
{
    return {t};
}

template<typename T>
class selector
{
public:
    selector(HDC dc, T t)
        : dc(dc), old(T(SelectObject(dc, t)))
    {}
    selector(selector&& src) noexcept
        : dc(src.dc), old(src.old)
    {
        src.dc = NULL;
        src.old = NULL;
    }
    ~selector()
    {
        SelectObject(dc, old);
    }
private:
    HDC dc;
    T old;
};

template<typename T>
selector<T> select(HDC dc, T t)
{
    return {dc, t};
}

class text_bk_mode_selector
{
public:
    text_bk_mode_selector(HDC dc, int mode)
        : dc(dc), old_mode(SetBkMode(dc, mode))
    {}
    text_bk_mode_selector(text_bk_mode_selector&& src) noexcept
        : dc(src.dc), old_mode(src.old_mode)
    {
        src.dc = nullptr;
        src.old_mode = 0;
    }
    ~text_bk_mode_selector()
    {
        SetBkMode(dc, old_mode);
    }
private:
    HDC dc;
    int old_mode;
};
class text_color_selector
{
public:
    text_color_selector(HDC dc,COLORREF color)
        :dc(dc),old(SetTextColor(dc,color))
    {}
    text_color_selector(text_color_selector&& src) noexcept
        :dc(src.dc),old(src.old)
    {
        src.dc = 0;
        src.old = 0;
    }
    ~text_color_selector()
    {
        SetTextColor(dc,old);
    }
private:
    HDC dc;
    COLORREF old;
};

class text_bk_color_selector
{
public:
    text_bk_color_selector(HDC dc,COLORREF color)
    :dc(dc),old(SetBkColor(dc,color))
    {}
    text_bk_color_selector(text_bk_color_selector&& src) noexcept
    :dc(src.dc),old(src.old)
    {
        src.dc = 0;
        src.old = 0;
    }
    ~text_bk_color_selector()
    {
        SetBkColor(dc,old);
    }
private:
    HDC dc;
    COLORREF old;
};
#endif //SNAKER_RESOURCE_HANDLER_HPP
