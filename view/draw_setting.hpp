//
// Created by lizhe on 12/29/2016.
//

#ifndef SNAKER_DRAW_SETTING_HPP
#define SNAKER_DRAW_SETTING_HPP

#include "base_draw.hpp"
class draw_setting :private base_draw
{
public:
    draw_setting(int width,int height,settings const&setting)
        : base_draw(width,height),setting(setting)
    {}
    void draw(HDC dc)
    {
        draw_background(dc);
        draw_content(dc);
    }
private:
    void draw_background(HDC dc)
    {
        hexagen_grid_drawer(dc,width,height).draw();
    }
    void draw_content(HDC dc)
    {
        auto bk_mode = text_bk_mode_selector(dc,TRANSPARENT);
        draw_headline(dc);
        draw_controller_type(dc);
    }
    void draw_headline(HDC dc)
    {
        std::string headline = "settings";
        auto text_color = text_color_selector(dc, RGB(181, 35, 211));
        auto font_selector = select(dc, headline_font.handled);
        print_text(dc,headline,headline_content_begin());
    }
    void draw_controller_type(HDC dc)
    {
        auto text_color = text_color_selector(dc,RGB(216,32,86));
        auto font_selector = select(dc,button_font.handled);
        auto use_mouse = setting.use_mouse();
        if(use_mouse)
            print_text(dc,"use mouse",text_content_begin(0));
        else
            print_text(dc,"use keyboard",text_content_begin(0));
    }
    settings const& setting;
};
#endif //SNAKER_DRAW_SETTING_HPP
