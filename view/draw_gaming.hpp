//
// Created by lizhe on 12/29/2016.
//

#ifndef SNAKER_DRAW_GAMING_HPP
#define SNAKER_DRAW_GAMING_HPP

#include "../model/snaker_gaming_model.hpp"
#include "resource_handler.hpp"
#include "hexagen_grid_drawer.hpp"

class draw_gaming
{
public:
    draw_gaming(snaker_gaming_model const& model, int width, int height)
        : model(model), width(width), height(height)
    {}

    void draw(HDC dc)
    {
        draw_background(dc);
        draw_objects(dc);
        draw_header(dc);
    }
private:
    void draw_background(HDC dc)
    {
        hexagen_grid_drawer(dc, width, height).draw();
    }
    RECT client_rect() const
    {
        RECT ret;
        ret.left = ret.top = 0;
        ret.right = width;
        ret.bottom = height;
        return ret;
    }
    void draw_objects(HDC dc)
    {
        draw_foods(dc);
        draw_poisonous_grasses(dc);
        draw_landmines(dc);
        draw_player_snake(dc);
        if (model.is_game_over())
            print_game_over(dc);
        else if (model.print_pass())
            print_pass(dc);
    }
    void draw_foods(HDC dc)
    {
        for (auto const& food:model.foods())
            draw_food(dc, food);
    }
    void draw_food(HDC dc, food_object const& food)
    {
        auto outline = food.outline().surounding_rect();
        size_type outer_size = outline.size;
        auto mem_dc = handle(CreateCompatibleDC(dc));
        auto bitmap = handle(CreateCompatibleBitmap(dc, outer_size.x, outer_size.y));
        auto bitmap_selector = select(mem_dc.handled, bitmap.handled);
        draw_shaded_circle(mem_dc.handled, food.outline() - outline.top_left, food.inner_radius, food.color());
        alpha_blend(dc, outline, mem_dc.handled);
        //        BitBlt(dc, dest_top_left.x, dest_top_left.y, outer_size.x, outer_size.y, mem_dc.handled, 0, 0, SRCCOPY);
        //        add_color(dc,outline,mem_dc.handled);
    }
    void draw_shaded_circle(HDC dc, circle c, double inner_radius, color_t inner_color)
    {
        uint16_t inner_alpha = 225;
        uint16_t outer_alpha = 0;
        uint16_t background_color = 0;
        auto foreground_color = inner_color * (inner_alpha / 255.0);
        uint16_t foreground_r = uint16_t(foreground_color.r) << 8, foreground_g = uint16_t(foreground_color.g) << 8,
            foreground_b = uint16_t(foreground_color.b) << 8;
        constexpr auto degree_step = 15;
        constexpr double pi = 3.1415926535898;
        auto center_pos = c.center;
        auto prev_inner_pos = center_pos + pos_on_circle(0, inner_radius);
        auto prev_outer_pos = center_pos + pos_on_circle(0, c.radius);
        for (int angle_in_degree = degree_step; angle_in_degree <= 360; angle_in_degree += degree_step)
        {
            double angle = double(angle_in_degree) / 180 * pi;
            auto inner_pos = center_pos + pos_on_circle(angle, inner_radius);
            auto outer_pos = center_pos + pos_on_circle(angle, c.radius);
            TRIVERTEX center =
                {
                    (int) center_pos.x, (int) center_pos.y,
                    foreground_r, foreground_g, foreground_b, uint16_t(inner_alpha << 8)
                }, prev_inner =
                {
                    (int) prev_inner_pos.x, (int) prev_inner_pos.y,
                    foreground_r, foreground_g, foreground_b, uint16_t(inner_alpha << 8)
                },
                prev_outer =
                {
                    (int) prev_outer_pos.x, (int) prev_outer_pos.y,
                    background_color, background_color, background_color, uint16_t(outer_alpha << 8)
                },
                inner =
                {
                    (int) inner_pos.x, (int) inner_pos.y,
                    foreground_r, foreground_g, foreground_b, uint16_t(inner_alpha << 8)
                },
                outer =
                {
                    (int) outer_pos.x, (int) outer_pos.y,
                    background_color, background_color, background_color, uint16_t(outer_alpha << 8)
                };
            TRIVERTEX triangle_verices[5] = {center,
                                             prev_inner,
                                             prev_outer,
                                             inner,
                                             outer};
            GRADIENT_TRIANGLE triangle_indices[3]{{0, 1, 3},
                                                  {1, 2, 3},
                                                  {2, 3, 4}};
            GradientFill(dc, &triangle_verices[0], 5, &triangle_indices[0], 3, GRADIENT_FILL_TRIANGLE);

            prev_inner_pos = inner_pos;
            prev_outer_pos = outer_pos;
        }
    }
    void alpha_blend(HDC dest_dc, rect dest_rect, HDC src_dc)
    {
        BLENDFUNCTION func;
        func.BlendOp = AC_SRC_OVER;
        func.BlendFlags = 0;
        func.SourceConstantAlpha = 255;
        func.AlphaFormat = AC_SRC_ALPHA;
        AlphaBlend(dest_dc, dest_rect.top_left.x, dest_rect.top_left.y, dest_rect.size.x, dest_rect.size.y, src_dc, 0, 0, dest_rect.size.x, dest_rect.size.y, func);
    }
    point pos_on_circle(double angle_in_radian, double radius)
    {
        return point{std::cos(angle_in_radian), std::sin(angle_in_radian)} * radius;
    }
    void draw_shaded_rect_at(HDC dc, rect outline, size_type inner_size, color_t inner_color)
    {
        auto inner_alpha = uint16_t(127 << 8);
        uint16_t outer_alpha = 0;
        uint16_t background_color = 0;
        auto foreground_color = inner_color * (inner_alpha / 255.0);
        uint16_t foreground_r = uint16_t(foreground_color.r) << 8, foreground_g = uint16_t(foreground_color.g) << 8,
            foreground_b = uint16_t(foreground_color.b) << 8;
        auto border = (outline.size - inner_size) * 0.5;
        auto top_left = outline.top_left;
        auto outer_size = outline.size;
        //        RECT rc{0,0,food.outer_size,food.outer_size};
        //        auto brush = handle(CreateSolidBrush(RGB(foreground_color.r,foreground_color.g,foreground_color.b)));
        //        FillRect(dc,&rc,brush.handled);
        int outer_top = top_left.y, outer_left = top_left.x,
            outer_bottom = outer_top + outer_size.y, outer_right = outer_left + outer_size.x,
            inner_top = outer_top + border.y, inner_left = outer_left + border.x,
            inner_bottom = inner_top + inner_size.y, inner_right = inner_left + inner_size.x;
        TRIVERTEX inner_topleft =
            {
                inner_left, inner_top,
                foreground_r,foreground_g,foreground_b, inner_alpha
            },
            inner_bottomright =
            {
                inner_right, inner_bottom,
                foreground_r, foreground_g, foreground_b, inner_alpha
            },
            inner_bottomleft =
            {
                inner_left, inner_bottom,
                foreground_r, foreground_g, foreground_b, inner_alpha
            },
            inner_topright =
            {
                inner_right, inner_top,
                foreground_r, foreground_g, foreground_b, inner_alpha
            },
            outer_topleft =
            {
                outer_left, outer_top,
                background_color, background_color, background_color, outer_alpha
            },
            outer_bottemright =
            {
                outer_right, outer_bottom,
                background_color, background_color, background_color, outer_alpha
            },
            outer_top_inner_left =
            {
                inner_left, outer_top,
                background_color, background_color, background_color, outer_alpha
            },
            inner_top_outer_left =
            {
                outer_left, inner_top,
                background_color, background_color, background_color, outer_alpha
            },
            inner_bottom_outer_right =
            {
                outer_right, inner_bottom,
                background_color, background_color, background_color, outer_alpha
            },
            outer_bottom_inner_right =
            {
                inner_right, outer_bottom,
                background_color, background_color, background_color, outer_alpha
            };
        auto horizontal_vertices = std::vector<TRIVERTEX>{inner_topleft, inner_bottomright, inner_top_outer_left, inner_bottomleft, inner_topright, inner_bottom_outer_right};
        auto horizontal_indices = std::vector<GRADIENT_RECT>{{0, 1},
                                                             {2, 3},
                                                             {4, 5}};
        GradientFill(dc, &horizontal_vertices[0], horizontal_vertices.size(), &horizontal_indices[0], horizontal_indices.size(), GRADIENT_FILL_RECT_H);

        auto vertical_vertices = std::vector<TRIVERTEX>{outer_top_inner_left, inner_topright, inner_bottomleft, outer_bottom_inner_right};
        auto vertical_indices = std::vector<GRADIENT_RECT>{{0, 1},
                                                           {2, 3}};
        GradientFill(dc, &vertical_vertices[0], vertical_vertices.size(), &vertical_indices[0], vertical_indices.size(), GRADIENT_FILL_RECT_V);

        TRIVERTEX outer_top_inner_right =
            {
                inner_right, outer_top,
                background_color, background_color, background_color, outer_alpha
            },
            outer_topright =
            {
                outer_right, outer_top,
                background_color, background_color, background_color, outer_alpha
            },
            inner_top_outer_right =
            {
                outer_right, inner_top,
                background_color, background_color, background_color, outer_alpha
            },
            inner_bottom_outer_left =
            {
                outer_left, inner_bottom,
                background_color, background_color, background_color, outer_alpha
            },
            outer_bottomleft =
            {
                outer_left, outer_bottom,
                background_color, background_color, background_color, outer_alpha
            },
            outer_bottom_inner_left =
            {
                inner_left, outer_bottom,
                background_color, background_color, background_color, outer_alpha
            };
        auto triangle_vertices = std::vector<TRIVERTEX>
            {
                outer_topleft, outer_top_inner_left, inner_topleft, inner_top_outer_left,
                outer_top_inner_right, outer_topright, inner_top_outer_right, inner_topright,
                inner_bottomright, inner_bottom_outer_right, outer_bottemright, outer_bottom_inner_right,
                inner_bottom_outer_left, inner_bottomleft, outer_bottom_inner_left, outer_bottomleft
            };
        auto triangle_indices = std::vector<GRADIENT_TRIANGLE>{{0,  1,  2},
                                                               {0,  2,  3},
                                                               {4,  5,  7},
                                                               {5,  6,  7},
                                                               {8,  9,  10},
                                                               {8,  10, 11},
                                                               {12, 13, 15},
                                                               {13, 14, 15}};
        GradientFill(dc, &triangle_vertices[0], triangle_vertices.size(), &triangle_indices[0], triangle_indices.size(), GRADIENT_FILL_TRIANGLE);
    }
    void draw_poisonous_grasses(HDC dc)
    {
        for (auto const& poisonous_grass:model.poisonous_grasses())
            draw_poisonous_grass(dc, poisonous_grass);
    }
    void draw_poisonous_grass(HDC dc, poisonous_grass_object const& grass)
    {
        if (!grass.hiden())
            draw_rect(dc, grass.outline(), grass.color);
    }
    void draw_landmines(HDC dc)
    {
        for (auto const& landmine:model.landmines())
            draw_landmine(dc, landmine);
    }
    void draw_landmine(HDC dc, landmine_object const& landmine)
    {
        auto color = landmine.color;
        auto pen = handle(CreatePen(PS_SOLID, 1, to_rgb(color)));
        auto brush = handle(CreateSolidBrush(to_rgb(color)));
        auto pen_selector = select(dc, pen.handled);
        auto brush_selector = select(dc, brush.handled);
        auto outer_radius = landmine.outer_radius;
        auto inner_radius = landmine.inner_radius;
        vector x_offset = {inner_radius / 3.0, 0},
            y_offset = {0, inner_radius / 3.0};
        point outer_topleft{-outer_radius, -outer_radius},
            outer_topright{outer_radius, -outer_radius},
            outer_bottomleft{-outer_radius, outer_radius},
            outer_bottomright{outer_radius, outer_radius},
            inner_topleft = {-inner_radius, -inner_radius},
            inner_topright = {inner_radius, -inner_radius},
            inner_bottomleft = {-inner_radius, inner_radius},
            inner_bottomright = {inner_radius, inner_radius};
        std::array<point, 24> vertices =
            {
                outer_topleft, inner_topleft + x_offset, inner_topleft + x_offset * 2,
                point{0, -outer_radius}, inner_topright - x_offset * 2, inner_topright - x_offset,
                outer_topright, inner_topright + y_offset, inner_topright + y_offset * 2,
                point{outer_radius, 0}, inner_bottomright - y_offset * 2, inner_bottomright - y_offset,
                outer_bottomright, inner_bottomright - x_offset, inner_bottomright - x_offset * 2,
                point{0, outer_radius}, inner_bottomleft + x_offset * 2, inner_bottomleft + x_offset,
                outer_bottomleft, inner_bottomleft - y_offset, inner_bottomleft - y_offset * 2,
                point{-outer_radius, 0}, inner_topleft + y_offset * 2, inner_topleft + y_offset
            };
        for (auto& p:vertices)
            p += landmine.position;
        auto polygon_vertices = to_points(vertices);
        Polygon(dc, &polygon_vertices[0], polygon_vertices.size());
    }
    template<std::size_t N>
    std::array<POINT, N> to_points(std::array<point, N> const& a)
    {
        std::array<POINT, N> ret;
        for (std::size_t i = 0; i < N; ++i)
            ret[i] = {(long) a[i].x, (long) a[i].y};
        return ret;
    }
    COLORREF to_rgb(color_t c)
    {
        return RGB(c.r, c.g, c.b);
    }
    void draw_player_snake(HDC dc)
    {
        draw_snake(dc, *model.player_snake());
    }
    void draw_snake(HDC dc, snake_object const& snake)
    {
        auto color = snake.color_;
        auto outer_radius = snake.outer_radius;
        auto inner_radius = snake.inner_radius;
        //        {
        //            auto range = model.player_snake_.eating_range();
        //            draw_circle(dc,range.center,range.radius);
        //        }
        for (auto const& body:snake.bodies())
        {
            draw_snake_body(dc, body, outer_radius, inner_radius, color);
        }
    }
    void draw_snake_body(HDC dc, snake_body const& body, double outer_radius, double inner_radius, color_t c)
    {
        auto outline = body.outline(outer_radius).surounding_rect();
        size_type outer_size = outline.size;
        auto mem_dc = handle(CreateCompatibleDC(dc));
        auto bitmap = handle(CreateCompatibleBitmap(dc, outer_size.x, outer_size.y));
        auto bitmap_selector = select(mem_dc.handled, bitmap.handled);
        draw_shaded_circle(mem_dc.handled, body.outline(outer_radius) - outline.top_left, inner_radius, c);
        alpha_blend(dc, outline, mem_dc.handled);
    }
    void draw_header(HDC dc)
    {
        print_score(dc);
        print_mission(dc);
    }
    void print_score(HDC dc)
    {
        std::string text = "score: " + std::to_string(model.player_snake()->bodies().size());
        auto bk_mode = text_bk_mode_selector(dc, TRANSPARENT);

        TextOut(dc, 50, 50, text.c_str(), text.size());
    }
    void print_mission(HDC dc)
    {
        std::string text = "mission: " + std::to_string(model.mission());
        auto bkmode = text_bk_mode_selector(dc, TRANSPARENT);
        TextOut(dc, 50, 80, text.c_str(), text.size());
    }
    void draw_circle(HDC dc, point position, double radius)
    {
        Ellipse(dc, position.x - radius, position.y - radius,
                position.x + radius, position.y + radius);
    }
    void draw_rect(HDC dc, rect r, color_t c)
    {
        auto pen = handle(CreatePen(PS_SOLID, 1, to_rgb(c)));
        auto brush = handle(CreateSolidBrush(to_rgb(c)));
        auto pen_selector = select(dc, pen.handled);
        RECT rc;
        rc.left = r.top_left.x;
        rc.top = r.top_left.y;
        rc.right = r.bottem_right().x;
        rc.bottom = r.bottem_right().y;
        FillRect(dc, &rc, brush.handled);
    }
    void print_game_over(HDC dc)
    {
        print_big_text(dc, "Game Over");
    }
    void print_pass(HDC dc)
    {
        print_big_text(dc, "You Win!");
    }
    void print_big_text(HDC dc, std::string const& text)
    {
        auto text_color = text_color_selector(dc, RGB(255, 0, 0));
        auto bk_mode = text_bk_mode_selector(dc, TRANSPARENT);
        RECT rc = client_rect();
        RECT new_rc;
        new_rc.left = interpolate(rc.left, rc.right, 2.0 / 3);
        new_rc.right = interpolate(rc.left, rc.right, 1.0 / 3);
        new_rc.top = interpolate(rc.top, rc.bottom, 2.0 / 3);
        new_rc.bottom = interpolate(rc.top, rc.bottom, 1.0 / 3);
        auto font = handle(CreateFont(new_rc.bottom - new_rc.top,
                                      0,
                                      0,
                                      0,
                                      FW_HEAVY,
                                      false,
                                      false,
                                      false,
                                      DEFAULT_CHARSET,
                                      OUT_DEFAULT_PRECIS,
                                      CLIP_DEFAULT_PRECIS,
                                      DEFAULT_QUALITY,
                                      DEFAULT_PITCH,
                                      "Consolas"));
        auto font_selector = select(dc, font.handled);
        DrawText(dc, text.c_str(), text.size(), &new_rc, DT_CENTER | DT_NOCLIP);

    }
    snaker_gaming_model const& model;
    const int width, height;

    static constexpr double sqrt_3 = std::sqrt(3);
    static constexpr double sin_60 = sqrt_3 / 2.0;
    static constexpr double sin_30 = 0.5;
};

#endif //SNAKER_DRAW_GAMING_HPP
