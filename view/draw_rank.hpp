//
// Created by lizhe on 12/29/2016.
//

#ifndef SNAKER_DRAW_RANK_HPP
#define SNAKER_DRAW_RANK_HPP

#include "base_draw.hpp"
#include "../rank/rank_manager.hpp"
class draw_rank:private base_draw
{
public:
    draw_rank(rank_manager const& rank,int width,int height)
        :base_draw(width,height),rank(rank)
    {}
    void draw(HDC dc)
    {
        draw_background(dc);
        draw_content(dc);
    }
private:
    void draw_background(HDC dc)
    {
        hexagen_grid_drawer(dc,width,height).draw();
    }
    void draw_content(HDC dc)
    {
        auto bk_mode = text_bk_mode_selector(dc,TRANSPARENT);
        draw_headline(dc);
        draw_ranks(dc);
    }
    void draw_headline(HDC dc)
    {
        std::string headline = "rank";
        auto text_color = text_color_selector(dc,RGB(181,35,211));
        auto font_selelctor = select(dc,headline_font.handled);
        print_text(dc,headline,headline_content_begin());
    }
    void draw_ranks(HDC dc)
    {
        auto text_color = text_color_selector(dc,RGB(216,31,86));
        auto font_selector = select(dc,button_font.handled);
        auto scores = rank.scores();
        for(unsigned int i =0;i < scores.size();++i)
        {
            std::string text = std::string("第") + std::to_string(i+1) + "名: ";
            text += std::to_string(scores[i]);
            print_text(dc,text,text_content_begin(i));
        }
    }
    rank_manager const& rank;
};
#endif //SNAKER_DRAW_RANK_HPP
