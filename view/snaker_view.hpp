//
// Created by lizhe on 12/14/2016.
//

#ifndef SNAKER_VIEW_HPP
#define SNAKER_VIEW_HPP

#include <vector>
#include <cmath>
#include "resource_handler.hpp"
#include "draw_gaming.hpp"
#include "draw_home.hpp"
#include "draw_rank.hpp"
#include "draw_setting.hpp"
#include "../model/game_page.hpp"


class snaker_view
{
public:
    snaker_view(snaker_gaming_model const& gaming_model, snaker_home_model const& home_model, int width, int height, game_page::game_page const& page,settings const& set,rank_manager const& rank)
        : width(width), height(height), gaming(gaming_model, width, height), home(home_model, width, height), rank(rank,width,height),setting(width,height,set),page(page)
    {}
    void set_window(HWND hwindow)
    {
        window = hwindow;
    }
    void draw()
    {
        PAINTSTRUCT ps;
        HDC dc = BeginPaint(window, &ps);
        auto mem_dc = handle(CreateCompatibleDC(dc));
        auto bitmap = handle(CreateCompatibleBitmap(dc, width, height));
        auto bitmap_selector = select(mem_dc.handled, bitmap.handled);
        draw_on_memory(mem_dc.handled);
        RECT rc = client_rect();
        BitBlt(dc, rc.left, rc.top, width, height, mem_dc.handled, 0, 0, SRCCOPY);
        EndPaint(window, &ps);
    }
private:
    RECT client_rect() const
    {
        RECT ret;
        ret.left = ret.top = 0;
        ret.right = width;
        ret.bottom = height;
        return ret;
    }
    void draw_on_memory(HDC dc)
    {
        if (page.is_gaming())
            gaming.draw(dc);
        else if(page.is_home())
            home.draw(dc);
        else if(page.is_rank())
            rank.draw(dc);
        else if(page.is_setting())
            setting.draw(dc);
    }
    HWND window = 0;
    const int width, height;
    draw_gaming gaming;
    draw_home home;
    draw_rank rank;
    draw_setting setting;
    game_page::game_page const& page;
};

#endif //SNAKER_VIEW_HPP
