//
// Created by lizhe on 12/29/2016.
//

#ifndef SNAKER_DRAW_HOME_HPP
#define SNAKER_DRAW_HOME_HPP

#include "resource_handler.hpp"
#include "../model/snaker_home_model.hpp"
#include "hexagen_grid_drawer.hpp"
#include "base_draw.hpp"

class draw_home: private base_draw
{
public:
    draw_home(snaker_home_model const& model, int width, int height)
        : base_draw(width,height),model(model)
    {}
    void draw(HDC dc)
    {
        draw_background(dc);
        draw_content(dc);
    }

private:
    void draw_background(HDC dc)
    {
        hexagen_grid_drawer(dc, width, height).draw();
    }
    void draw_content(HDC dc)
    {
        auto bk_mode = text_bk_mode_selector(dc, TRANSPARENT);
        draw_headline(dc);
        draw_buttons(dc);
    }
    void draw_headline(HDC dc)
    {
        std::string headline = "snaker";
        auto text_color = text_color_selector(dc, RGB(181, 35, 211));
        auto font_selector = select(dc, headline_font.handled);
        print_text(dc,headline,headline_content_begin());
    }
    void draw_buttons(HDC dc)
    {
        auto text_color = text_color_selector(dc,RGB(216,31,86));
        auto font_selector = select(dc, button_font.handled);
        for(int i = 0 ; i < model.number_of_buttons();++i)
        {
            auto button = model.to_button(i);
            auto text = model.button_text(button);
            if(model.selected(button))
                print_selected_text(dc,text,text_content_begin(i));
            else
                print_text(dc,text,text_content_begin(i));
        }
    }
    snaker_home_model const& model;
};

#endif //SNAKER_DRAW_HOME_HPP
