//
// Created by lizhe on 1/3/2017.
//

#ifndef SNAKER_BASE_DRAW_HPP
#define SNAKER_BASE_DRAW_HPP

#include <string>
#include "resource_handler.hpp"
#include "../model/snaker_home_model.hpp"
#include "hexagen_grid_drawer.hpp"

class base_draw
{
public:
    base_draw(int width, int height)
        : width(width), height(height)
    {}

protected:
    void print_text(HDC dc,std::string const& text,int top)
    {
        RECT rect   {0,0,width,height};
        rect.top = top;
        DrawText(dc,text.c_str(),text.size(),&rect,DT_CENTER | DT_NOCLIP);
    }
    void print_selected_text(HDC dc,std::string const& text,int top)
    {
        auto bk_mode = text_bk_mode_selector(dc,OPAQUE);
        auto bk_color = text_bk_color_selector(dc,selected_text_bk_color);
        print_text(dc,text,top);
    }
    int headline_content_begin() const
    {
        return headline_border;
    }
    int headline_border_end() const
    {
        return headline_content_begin() + headline_content_size + headline_border;
    }
    int text_content_begin() const
    {
        return headline_border_end() + button_border;
    }
    int text_content_begin(int nth_button)
    {
        return text_content_begin() + (2 * button_border + button_content_size) * nth_button;
    }
    const int width;
    const int height;
    static const int headline_border = 30;
    static const int headline_content_size = 200;
    static const int button_border = 30;
    static const int button_content_size = 60;
    static const COLORREF selected_text_bk_color = RGB(205,194,180);
    handler<HFONT> headline_font = create_font(headline_content_size);
    handler<HFONT> button_font = create_font(button_content_size);
    handler<HFONT> create_font(int size) const
    {
        return handle(CreateFont(size,
                                 0,
                                 0,
                                 0,
                                 FW_HEAVY,
                                 false,
                                 false,
                                 false,
                                 DEFAULT_CHARSET,
                                 OUT_DEFAULT_PRECIS,
                                 CLIP_DEFAULT_PRECIS,
                                 DEFAULT_QUALITY,
                                 DEFAULT_PITCH,
                                 "Consolas"));
    }
};
#endif //SNAKER_BASE_DRAW_HPP
