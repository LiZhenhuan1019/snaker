//
// Created by lizhe on 12/29/2016.
//

#ifndef SNAKER_HEXAGEN_GRID_DRAWER_HPP
#define SNAKER_HEXAGEN_GRID_DRAWER_HPP

#include "../model/geometry_utililty.hpp"
#include "resource_handler.hpp"
class hexagen_grid_drawer
{
public:
    hexagen_grid_drawer(HDC dc,int width,int height)
        :dc(dc),width(width),height(height)
    {}
    void draw()
    {

        auto bkground_brush = handle(CreateSolidBrush(RGB(240, 240, 240)));
        auto background_brush_selector = select(dc, bkground_brush.handled);
        RECT rc = client_rect();
        FillRect(dc, &rc, bkground_brush.handled);
        draw_grid(dc);
    }
private:
    RECT client_rect() const
    {
        RECT ret;
        ret.left = ret.top = 0;
        ret.right = width;
        ret.bottom = height;
        return ret;
    }
    void draw_grid(HDC dc)
    {
        constexpr double size_length = 18;
        constexpr double gap_size = 4;
        constexpr double x_offset = size_length * sqrt_3 + gap_size;
        constexpr double y_offset = size_length * 3.0 / 2 + gap_size / sin_60;
        auto pen_color = RGB(210,210,210);
        auto pen = handle(CreatePen(PS_SOLID,1,pen_color));
        auto pen_selector = select(dc,pen.handled);
        for(double x = -x_offset;x <= width + x_offset;x+=x_offset)
        {
            int i = 0;
            for(double y = -x_offset;y<=height + y_offset;y+=y_offset,++i)
                draw_hexsagen(dc,point{x + i%2 * x_offset / 2,y},size_length);
        }
    }
    void draw_hexsagen(HDC dc, point center, double size_length)
    {
        MoveToEx(dc, center.x, center.y - size_length, NULL);
        LineTo(dc, center.x + size_length * sin_60, center.y - size_length * sin_30);
        LineTo(dc, center.x + size_length * sin_60, center.y + size_length * sin_30);
        LineTo(dc, center.x, center.y + size_length);
        LineTo(dc, center.x - size_length * sin_60, center.y + size_length * sin_30);
        LineTo(dc, center.x - size_length * sin_60, center.y - size_length * sin_30);
        LineTo(dc, center.x, center.y - size_length);
    }
    HDC dc;
    const int width,height;
    static constexpr double sqrt_3 = std::sqrt(3);
    static constexpr double sin_60 = sqrt_3 / 2.0;
    static constexpr double sin_30 = 0.5;
};
#endif //SNAKER_HEXAGEN_GRID_DRAWER_HPP
