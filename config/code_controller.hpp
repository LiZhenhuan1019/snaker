//
// Created by lizhe on 2016/10/27.
//

#ifndef SIMPLE_C_INTERPRETER_CODE_CONTROLLER_H
#define SIMPLE_C_INTERPRETER_CODE_CONTROLLER_H

#include <vector>
#include <string>

struct code_pos
{
    std::size_t line_num = 0;
    std::size_t column_num = 0;
};

class code_controller
{
public:

    explicit code_controller(std::string const& code)
    {
        std::size_t begin_pos = 0;
        std::size_t newline_pos = code.find('\n', begin_pos);
        std::size_t count;
        codes.push_back(" ");//line begin from one.
        if (newline_pos == code.npos)
            count = code.npos;
        else
            count = newline_pos + 1 - begin_pos;//add one to include '\n'
        auto str = code.substr(begin_pos, count);
        while (newline_pos != code.npos)
        {
            str[newline_pos - begin_pos] = ' ';//changge '\n' to ' '
            //std::cout << str;
            codes.push_back(str);
            begin_pos = newline_pos + 1;
            newline_pos = code.find('\n', begin_pos);
            std::size_t count;
            if (newline_pos == code.npos)
                count = code.npos;
            else
                count = newline_pos + 1 - begin_pos;
            str = code.substr(begin_pos, count);
        }
        codes.push_back(str);
        //std::cout<<"constructor end \n";
    }
    code_controller(code_controller const&) = delete;
    char current() const
    {
        return codes[pos.line_num][pos.column_num];
    }
    bool eat()//如果换行了,返回true
    {
        if (reach_end_of_line())
            if (reach_last_line())
                confirm_reach_end();
            else
            {
                go_to_next_line();
                return true;
            }
        else
            go_to_next_char();
        return false;
    }
    bool skip_space()//如果换行了,返回true
    {
        bool next_line = false;
        while (is_space())
            next_line = next_line | eat();
        return next_line;
    }
    char current_and_eat()
    {
        char c = current();
        eat();
        return c;
    }
    char skipping_space_current_and_eat()
    {
        skip_space();
        return current_and_eat();
    }
    char peek_skipping_space_and_get_current()
    {
        code_pos pre_pos = pos;
        char cur = skipping_space_current_and_eat();
        pos = pre_pos;
        return cur;
    }
    bool reach_end() // only be true after confirm_reach_end() is called, ie. after the execution of the inner-most if branch in eat().
    {
        skip_space();
        return pos.column_num == codes[pos.line_num].size();
    }

    code_pos& current_pos()
    {
        return pos;
    }
    code_pos const& current_pos() const
    {
        return pos;
    }
    code_pos replace_pos(code_pos new_pos)
    {
        code_pos pre_pos = pos;
        pos = new_pos;
        return pre_pos;
    }
    std::size_t line_number() const
    {
        return pos.line_num;
    }

private:
    bool reach_end_of_line() const
    {
        return pos.column_num >= codes[pos.line_num].size() - 1;
    }
    bool reach_last_line() const
    {
        return pos.line_num == codes.size() - 1;
    }
    bool is_space() const
    {
        char c = current();
        return c == ' ' || c == '\t';
    }
    void confirm_reach_end()
    {
        pos.column_num = codes[pos.line_num].size();
    }
    void go_to_next_line()
    {
        ++pos.line_num;
        pos.column_num = 0;
    }
    void go_to_next_char()
    {
        ++pos.column_num;
    }
    std::vector<std::string> codes;
    code_pos pos;
};

#endif //SIMPLE_C_INTERPRETER_CODE_CONTROLLER_H
