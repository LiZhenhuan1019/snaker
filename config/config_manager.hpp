//
// Created by lizhe on 11/30/2016.
//

#ifndef SNAKER_CONFIG_MANAGER_HPP
#define SNAKER_CONFIG_MANAGER_HPP


#include "settings.hpp"
#include "code_controller.hpp"
#include "config_loader.hpp"
#include "config_writer.hpp"

class config_manager
{
public:
    explicit config_manager(settings& setting,std::string config_file_name = "snaker_setting.json")
        : config_file_name(config_file_name),setting(setting)
    {
        if(std::ifstream(config_file_name))
            load_config(setting);
        else
            write_config(setting);
    }
    void load_config(settings& setting)
    {
        config_loader(config_file_name, setting);
    }
    void write_config(settings const& setting)
    {
        config_writer(config_file_name,setting);
    }
private:
    std::string config_file_name;
    settings& setting;
};

#endif //SNAKER_CONFIG_MANAGER_HPP
