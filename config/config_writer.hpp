//
// Created by lizhe on 12/29/2016.
//

#ifndef SNAKER_CONFIG_WRITER_HPP
#define SNAKER_CONFIG_WRITER_HPP

#include <string>
#include <fstream>
#include <stdexcept>
#include "settings.hpp"
class config_writer
{
public:
    config_writer(std::string config_file_name,settings const& setting)
        :config_file_name(config_file_name),out(config_file_name),setting(setting)
    {
        if(out.bad())
            throw std::invalid_argument(std::string("cannot open file '") + config_file_name + "' to write !");
        write_config();
    }
private:
    void write_config()
    {
        write_snaker();
    }
    void write_snaker()
    {
        out<<open_curly_brace() + end_line();
        write_spaces(2);
        out<<R"~("snaker":  )~";
        write_aspects(4);
        out<<end_line() + close_curly_brace();
    }
    void write_aspects(int spaces)
    {
        out<<open_curly_brace() + end_line();
        write_spaces(spaces);
        out<<R"~("settings":  )~";
        write_settings(spaces + 2);
        out<<end_line();
        write_spaces(spaces - 2);
        out<<close_curly_brace();
    }
    void write_settings(int spaces)
    {
        out<<open_curly_brace() + end_line();
        write_spaces(spaces);
        out<<R"~("input_mapping":  )~";
        write_input_mapping(spaces + 2);
        out<<write_next() + end_line();
        write_spaces(spaces);
        out<<R"~("controller":  )~";
        write_controller(spaces + 2);
        out<<end_line();
        write_spaces(spaces - 2);
        out<<close_curly_brace();
    }
    void write_input_mapping(int spaces)
    {
        out<<open_curly_brace() + end_line();
        auto const& map = setting.get_input_mapping().mapping();
        auto iter = map.begin();
        write_spaces(spaces);
        out<<std::string("\"") + iter->second.name() + "\":  ";
        out<<std::string("\"") + std::to_string(iter->first) + "\"";
        for(++iter; iter != map.end(); ++ iter)
        {
            out<<",\n";
            write_spaces(spaces);
            out <<std::string("\"") + iter->second.name() + "\":  ";
            out<<std::string("\"") + std::to_string(iter->first) + "\"";
        }
        out<<end_line();
        write_spaces(spaces -2);
        out<< close_curly_brace();
    }
    void write_controller(int spaces)
    {
        out<<open_curly_brace() + end_line();
        bool use_mouse = setting.use_mouse();
        write_spaces(spaces);
        out<<std::string(R"~("use_mouse":  )~") + "\"" + std::to_string((int)use_mouse) + "\"";
        out<<end_line();
        write_spaces(spaces - 2);
        out<<close_curly_brace();
    }
    void write_spaces(int n)
    {
        for(int i = 0; i < n ;++i)
            out<<" ";
    }
    std::string open_curly_brace() const
    {
        return "{";
    }
    std::string close_curly_brace() const
    {
        return "}";
    }
    std::string write_next() const
    {
        return ",";
    }
    std::string end_line() const
    {
        return "\n";
    }
    std::string config_file_name;
    std::ofstream out;
    settings const& setting;
};
#endif //SNAKER_CONFIG_WRITER_HPP
