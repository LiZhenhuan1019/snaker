//
// Created by lizhe on 12/1/2016.
//

#ifndef SNAKER_CONFIG_LOADER_HPP
#define SNAKER_CONFIG_LOADER_HPP

#include <string>
#include <fstream>
#include <stdexcept>
#include <optional>
#include "settings.hpp"
#include "code_controller.hpp"

class key_name_to_value
{
    friend bool operator<(key_name_to_value const& lhs, key_name_to_value const& rhs);
public:
    explicit key_name_to_value(std::string const& name)
        : name(name)
    {
        hex_number();
    }
    unsigned long long value()
    {
        return key_value;
    }
private:
    void hex_number()
    {
        key_value = std::stoi(name, nullptr, 10);
    }
    std::string name;
    unsigned long long key_value;
};

inline bool operator<(key_name_to_value const& lhs, key_name_to_value const& rhs)
{
    return lhs.key_value < rhs.key_value;
}

class config_loader
{
public:
    config_loader(std::string config_file_name, settings& setting)
        : config_file_name(config_file_name), code(load_file()), setting(setting)
    {
        read_config();
    }

private:
    std::string load_file()
    {
        std::ifstream config_file(config_file_name);
        if (config_file.bad())
            throw std::invalid_argument(std::string("cannot open config file '") + config_file_name + "'!");
        std::string line;
        std::string file_string;
        while (config_file.good())
        {
            std::getline(config_file, line);
            line.push_back('\n');
            file_string += line;
        }
        return file_string;
    }
    void read_config()
    {
        while (!code.reach_end())
            read_snaker();
    }
    void read_snaker()
    {
        enforce_read_char('{');
        auto str = enforce_read_string_literal();
        if (str == "snaker")
        {
            enforce_read_char(':');
            read_aspects();
        }
        enforce_read_char('}');
    }
    void read_aspects()
    {
        do
        {
            enforce_read_char('{');
            auto str = enforce_read_string_literal();
            if (str == "settings")
            {
                enforce_read_char(':');
                read_settings();
            }
            enforce_read_char('}');
        } while (read_char(','));
    }
    void read_settings()
    {
        enforce_read_char('{');
        do
        {
            auto str = enforce_read_string_literal();
            if (str == "input_mapping")
            {
                enforce_read_char(':');
                read_input_mapping();
            }
            if (str == "controller")
            {
                enforce_read_char(':');
                read_controller();
            }
        } while (read_char(','));
        enforce_read_char('}');
    }
    void read_input_mapping()
    {
        enforce_read_char('{');
        do
        {
            auto action_name = enforce_read_string_literal();
            enforce_read_char(':');
            auto key = enforce_read_string_literal();
            setting.set_input_key(action_name, key_name_to_value(key).value());
        } while (read_char(','));
        enforce_read_char('}');
    }
    void read_controller()
    {
        enforce_read_char('{');
        do
        {
            auto str = enforce_read_string_literal();
            if (str == "use_mouse")
            {
                enforce_read_char(':');
                read_use_mouse();
            }
        } while (read_char(','));
        enforce_read_char('}');
    }
    void read_use_mouse()
    {
        auto str = enforce_read_string_literal();
        bool use_mouse = (bool)std::stoi(str,nullptr);
        setting.set_use_mouse(use_mouse);
    }
    bool read_char(char c)
    {
        code_pos char_pos = code.current_pos();
        if (code.skipping_space_current_and_eat() == c)
            return true;
        else
        {
            code.replace_pos(char_pos);
            return false;
        }
    }
    template<typename Pred>
    std::optional<char> read_char(Pred pred)
    {
        code_pos char_pos = code.current_pos();
        char c = code.skipping_space_current_and_eat();
        if (pred(c))
            return c;
        else
        {
            code.replace_pos(char_pos);
            return std::nullopt;
        }
    }
    void enforce_read_char(char c)
    {
        if (!read_char(c))
            throw std::domain_error(std::string("Interpret config file failed!") + "In " + __func__);
    }
    std::optional<std::string> read_identifier()
    {
        code_pos id_pos = code.current_pos();
        if (auto opt = read_char(begin_id_char))
        {
            std::string name;
            name.push_back(opt.value());
            while (opt = read_char(is_id_char))
                name.push_back(opt.value());
            return name;
        }
        code.replace_pos(id_pos);
        return std::nullopt;
    }
    std::optional<int> read_int_literal()
    {
        if (auto opt = read_char(isdigit))
        {
            std::string digits;
            digits.push_back(opt.value());
            while (opt = read_char(isdigit))
                digits.push_back(opt.value());
            return std::stoi(digits);
        }
        return nullopt;
    }
    optional<std::string> read_string_literal()
    {
        if (read_char('"'))
        {
            std::string literal;
            while (auto opt = read_char([](char c)
                                        { return c != '"'; }))
                literal.push_back(opt.value());
            enforce_read_char('"');
            return literal;
        }
        return nullopt;
    }
    std::string enforce_read_string_literal()
    {
        if (auto opt = read_string_literal())
            return opt.value();
        else
            throw std::domain_error(std::string("Interpret config file failed!Expect identifier in config file.In ") + __func__);
    }

    static bool begin_id_char(char c)
    {
        return std::isalpha(c) || c == '_';
    }
    static bool is_id_char(char c)
    {
        return std::isalnum(c) || c == '_';
    }
    static bool is_digit(char c)
    {
        return std::isdigit(c);
    }
    std::string config_file_name;
    code_controller code;
    settings& setting;
};

#endif //SNAKER_CONFIG_LOADER_HPP
