
//
// Created by lizhe on 11/30/2016.
//

#ifndef SNAKER_SETTINGS_HPP
#define SNAKER_SETTINGS_HPP

#include <algorithm>
#include <stdexcept>
#include "../controller/input_mapping.hpp"

class settings
{
public:
    settings()
    {
        initialize_input_mapping();
    }
    input_mapping& get_input_mapping()
    {
        return input;
    }
    input_mapping const& get_input_mapping() const
    {
        return input;
    }
    bool use_mouse() const
    {
        return use_mouse_;
    }
    void set_use_mouse(bool b)
    {
        use_mouse_ = b;
    }

    void set_input_key(std::string action_name, input_mapping::key_type key)
    {
        input.set(action(action_name), key);
    }
private:
    void initialize_input_mapping()
    {
        input.set(action("up"),0x57);
        input.set(action("down"),0x53);
        input.set(action("left"),0x41);
        input.set(action("right"),0x44);
        input.set(action("accelerate"),0x20);
    }
    input_mapping input;
    bool use_mouse_ = false;
};

#endif //SNAKER_SETTINGS_HPP
