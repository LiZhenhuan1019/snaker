//
// Created by lizhe on 12/14/2016.
//

#ifndef SNAKER_FORWARD_LIST_HPP
#define SNAKER_FORWARD_LIST_HPP

#include <memory>
#include <cassert>

template <typename ValueT>
class list_node
{
public:
    using value_t = ValueT;

    list_node(value_t const& v, list_node *prev, std::unique_ptr<list_node> next)
        : value(v), prev(prev), next(std::move(next))
    {}
    value_t value;
    list_node *prev;
    std::unique_ptr<list_node> next;
};

template <typename ValueT>
class list
{
public:
    using value_t = ValueT;
    using node_t = list_node<value_t>;

    class iterator;

    class const_iterator
    {
        friend class iterator;

        friend class list;

        const_iterator(const list *l, node_t *p)
            : plist(l), node(p)
        {}
    public:
        value_t const& operator*() const
        {
            return node->value;
        }
        value_t const *operator->() const
        {
            return &node->value;
        }

        const_iterator& operator++()
        {
            node = node->next.get();
            return *this;
        }
        const_iterator& operator++(int)
        {
            const_iterator ret = *this;
            ++*this;
            return ret;
        }
        const_iterator& operator--()
        {
            if (node == nullptr)
                node = plist->tail;
            else
                node = node->prev;
            return *this;
        }
        const_iterator operator--(int)
        {
            const_iterator ret = *this;
            --*this;
            return ret;
        }

        bool operator==(const_iterator iter) const
        {
            return node == iter.node;
        }
        bool operator!=(const_iterator iter) const
        {
            return !(*this == iter);
        }
    private:
        const list *plist;
        node_t *node;
    };

    class iterator
    {
        friend class list;

        iterator(list *l, node_t *p)
            : plist(l), node(p)
        {}
    public:
        operator const_iterator() const
        {
            return const_iterator(plist, node);
        }
        value_t& operator*() const
        {
            return node->value;
        }
        value_t *operator->() const
        {
            return &node->value;
        }

        iterator& operator++()
        {
            node = node->next.get();
            return *this;
        }
        iterator operator++(int)
        {
            iterator ret = *this;
            ++*this;
            return ret;
        }
        iterator& operator--()
        {
            if (node == nullptr)
                node = plist->tail;
            else
                node = node->prev;
            return *this;
        }
        iterator operator--(int)
        {
            iterator ret = *this;
            --*this;
            return ret;
        }

        bool operator==(iterator iter) const
        {
            return node == iter.node;
        }
        bool operator!=(iterator iter) const
        {
            return !(*this == iter);
        }
    private:
        list *plist;
        node_t *node;
    };


    iterator begin()
    {
        return get_iter(head.get());
    }
    const_iterator begin() const
    {
        return get_const_iter(head.get());
    }
    const_iterator cbegin() const
    {
        return begin();
    }

    iterator end()
    {
        return get_iter(nullptr);
    }
    const_iterator end() const
    {
        return get_const_iter(nullptr);
    }
    const_iterator cend() const
    {
        return end();
    }

    value_t& front()
    {
        return *begin();
    }
    value_t const& front() const
    {
        return *begin();
    }
    value_t& back()
    {
        return tail->value;
    }
    value_t const& back() const
    {
        return tail->value;
    }
    iterator insert(const_iterator iter, value_t const& value)
    {
        if (iter != end())
            return insert_before_end(iter, value);
        else
            return insert_at_end(value);
    }
    iterator remove(const_iterator iter)
    {
        if (iter.node->next)
            return remove_before_tail(iter);
        else
            return remove_tail();
    }
    void clear()
    {
        tail = nullptr;
        size_ = 0;
        head.reset();
    }
    std::size_t size() const
    {
        return size_;
    }
    bool empty() const
    {
        return size_ == 0;
    }
private:
    iterator get_iter(node_t *n)
    {
        return iterator(this, n);
    }
    const_iterator get_const_iter(node_t *n) const
    {
        return const_iterator(this, n);
    }
    iterator insert_before_end(const_iterator iter, value_t const& value)
    {
        if (iter.node->prev) // insert at middle
            return insert_at_middle(iter, value);
        else // insert at begin
        {
            return insert_at_begin(iter, value);
        }
    }
    iterator insert_at_middle(const_iterator iter, value_t const& value)
    {
        auto prev = iter.node->prev;
        prev->next = std::make_unique<node_t>(value, prev, std::move(prev->next));
        ++size_;
        iter.node->prev = prev->next.get();
        return get_iter(prev->next.get());
    }
    iterator insert_at_begin(const_iterator iter, value_t const& value) // before end
    {
        head = std::make_unique<node_t>(value, nullptr, std::move(head));
        ++size_;
        iter.node->prev = head.get();
        return begin();
    }
    iterator insert_at_end(value_t const& value)
    {
        if (!empty()) // not empty
        {
            tail->next = std::make_unique<node_t>(value, tail, nullptr);
            ++size_;
            tail = tail->next.get();
        }
        else //is empty
        {
            head = std::make_unique<node_t>(value, nullptr, nullptr);
            assert(size_ == 0);
            size_ = 1;
            tail = head.get();
        }
        return get_iter(tail);
    }
    iterator remove_before_tail(const_iterator iter)
    {
        if (iter.node->prev)
            return remove_at_middle(iter);
        else
            return remove_at_begin();
    }
    iterator remove_at_middle(const_iterator iter)
    {
        auto current = iter.node;
        auto prev = current->prev;
        auto next = std::move(current->next);
        next->prev = prev;
        prev->next = std::move(next);
        --size_;
        return get_iter(prev->next.get());
    }
    iterator remove_at_begin() //before tail
    {
        head = std::move(head->next);
        head->prev = nullptr;
        --size_;
        return begin();
    }
    iterator remove_tail()
    {
        auto prev = tail->prev;
        if (prev)
            prev->next.reset();
        else
        {
            assert(size_ == 1);
            head.reset();
        }
        tail = prev;
        --size_;
        return end();
    }
    std::unique_ptr<node_t> head;
    node_t *tail = nullptr;
    std::size_t size_ = 0;
};

#endif //SNAKER_FORWARD_LIST_HPP
