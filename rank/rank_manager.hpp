//
// Created by lizhe on 1/3/2017.
//

#ifndef SNAKER_RANK_MANAGER_HPP
#define SNAKER_RANK_MANAGER_HPP

#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include "../model/snaker_gaming_model.hpp"

class rank_manager
{
public:
    rank_manager(snaker_gaming_model const& model,std::string rank_file_name = "rank")
        : model(model),rank_file_name(rank_file_name)
    {
        read_score();
    }
    void update_rank(int score)
    {
        scores_.push_back(score);
        eliminate();
        write_file();
    }
    std::vector<int> scores() const
    {
        return scores_;
    }
private:
    void read_score()
    {
        read_file();
        eliminate();
    }
    void eliminate()
    {
        std::sort(scores_.begin(), scores_.end(),std::greater<int>());
        if (scores_.size() > max_rank_number)
            scores_.erase(scores_.begin() + max_rank_number, scores_.end());
    }
    void read_file()
    {
        std::ifstream file(rank_file_name);
        scores_.clear();
        int score = 0;
        while (file >> score)
            scores_.push_back(score);
    }
    void write_file()
    {
        std::ofstream file(rank_file_name);
        for(auto score:scores_)
            file<<score<< " ";
    }
    snaker_gaming_model const& model;
    std::string rank_file_name;
    std::vector<int> scores_;
    static const int max_rank_number = 5;
};

#endif //SNAKER_RANK_MANAGER_HPP
