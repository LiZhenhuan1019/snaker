//
// Created by lizhe on 12/14/2016.
//

#ifndef SNAKER_SNAKER_MODEL_HPP
#define SNAKER_SNAKER_MODEL_HPP

#include <random>
#include <ctime>
#include <iostream>
#include "../repository/indexed_repository.h"
#include "snake_object.hpp"
#include "food_object.hpp"
#include "landmine_object.hpp"
#include "poisonous_grass_object.hpp"

class snaker_gaming_model   // model when playing game.
{
    friend class model_generator;  // deserializer

    friend class model_saver;      // serializer

public:
    using snakes_t = lzhlib::indexed_repository<snake_object>;
    using snake_iterator_t = snakes_t::iterator;
    using snake_const_iterator_t = snakes_t::const_iterator;
    using foods_t = list<food_object>;
    using landmines_t = list<landmine_object>;
    using poisonous_grasses_t = list<poisonous_grass_object>;
    snaker_gaming_model(size_type map_size)
        : map_size_(map_size)
    {
        reset();
    }
    void reset()    // reset model : clear all objects and generate new ones.
    {
        clear_all();
        generate_player_snake();
        generate_landmines();
        generate_poisonous_grasses();
        generate_foods();
    }
    void update()   // update objects. make game progress.
    {
        if (!is_game_over())
        {
            update_snakes();
            check_collision();
            check_mission();
            update_step();
            update_objects(foods_);
            update_objects(poisonous_grasses_);
            update_objects(landmines_);
            generate_landmines();
            generate_foods();
        }
    }
    bool is_game_over() const
    {
        return is_game_over_;
    }
    bool print_pass() const  // whether text
    {
        return pass_ && win_displayed_step < max_win_displayed_step;
    }
    int mission() const
    {
        return mission_;
    }
    size_type map_size() const
    {
        return map_size_;
    }
    std::size_t score() const
    {
        return player_snake()->bodies().size();
    }
    snake_iterator_t player_snake()
    {
        return snakes.begin();
    }
    snake_const_iterator_t player_snake() const
    {
        return snakes.begin();
    }
    foods_t const &foods() const
    {
        return foods_;
    }
    landmines_t const &landmines() const
    {
        return landmines_;
    }
    poisonous_grasses_t const &poisonous_grasses() const
    {
        return poisonous_grasses_;
    }


private:
    void clear_all()
    {
        is_game_over_ = false;
        mission_ = 1;
        pass_ = false;
        win_displayed_step = 0;
        snakes.clear();
        foods_.clear();
        landmines_.clear();
        poisonous_grasses_.clear();
    }
    void generate_player_snake()
    {
        snakes.set_stock(0, snake_object{});
        for (auto i = 0; i < 10; i++)
        {
            player_snake()->add_body(point{150.0 - 5 * i, 150.0 - 5 * i});
        }
        player_snake()->head_direction(direction{1, 1});
        player_snake()->color_ = color_t{0, 255, 0};
        player_snake()->original_color_ = player_snake()->color_;
    }
    void generate_landmines()
    {
        while (landmines_.size() < max_landmines_number())
            generate_object(landmines_);
    }
    void generate_poisonous_grasses()
    {
        while (poisonous_grasses_.size() < max_poisonous_grasses_number())
            generate_object(poisonous_grasses_);
    }
    void generate_foods()
    {
        while (foods_.size() < max_foods_number())
            generate_object(foods_);
    }
    template <template <typename ...> class Container, typename Object>
    void generate_object(Container<Object> &objects)
    {
        int border = (int) Object::size;
        auto new_object = create_object<Object>(random_position(border));
        while (!position_is_allowed(new_object))
            new_object = create_object<Object>(random_position(border));
        objects.insert(objects.end(), new_object);
    }
    template <typename Object>
    Object create_object(point p)  // specialized below.
    {
        return Object{p};
    }
    template <typename object>
    bool position_is_allowed(object const &o)
    {
        for (auto const &landmine:landmines_)
            if (collides(o.outline(), landmine.outline()))
            {
                return false;
            }

        return true;
    }
    point random_position(int border)
    {
        std::uniform_int_distribution<> horizontal_dist(border, map_size_.x - border);
        std::uniform_int_distribution<> vertical_dist(border, map_size_.y - border);
        int x = horizontal_dist(random_engine);
        int y = vertical_dist(random_engine);
        assert(x >= border && x <= map_size_.x - border);
        assert(y >= border && y <= map_size_.y - border);
        return point{double(x), double(y)};
    }
    color_t random_color()
    {
        std::uniform_int_distribution<> color_dist(-127, 128);
        int offset = color_dist(random_engine);
        color_t c;
        switch (std::uniform_int_distribution<>(0, 2)(random_engine))
        {
        case 0:c = {(unsigned char) (127 + offset), (unsigned char) (127 - offset), (unsigned char) (127 - offset)};
            break;
        case 1:c = {(unsigned char) (127 - offset), (unsigned char) (127 + offset), (unsigned char) (127 - offset)};
            break;
        case 2:c = {(unsigned char) (127 - offset), (unsigned char) (127 - offset), (unsigned char) (127 + offset)};
            break;
        }
        return c;
    }
    void update_snakes()
    {
        update_player();
        //TODO: update ai-snakes
    }
    void update_player()
    {
        if (player_snake()->is_dead())
            game_over();
        else
        {
            player_snake()->basic_speed = basic_speed();
            player_snake()->move();
        }
    }
    void game_over()
    {
        is_game_over_ = true;
    }
    void check_collision()
    {
        //TODO: check collision between snakes here.
        check_snake_eating(player_snake(), foods_);
        check_snake_eating(player_snake(), poisonous_grasses_);
        check_snake_eating(player_snake(), landmines_);
        check_hitting_the_wall();
    }
    template <typename EatenList>
    void check_snake_eating(snake_iterator_t snake, EatenList &list)
    {
        for (auto &object : list)
        {
            if (can_eat(*snake, object))
                eat(snake, object);
        }
    }
    template <typename eaten_t>
    bool can_eat(snake_object const &snake, eaten_t const &eaten)
    {
        return eaten.is_active() && collides(snake.eating_range(), eaten.outline());
    }
    void eat(snake_iterator_t snake, food_object &food)
    {
        food.eaten(snake.index());
        snake->add_at_tail(1);
    }
    void eat(snake_iterator_t snake, poisonous_grass_object &poisonous_grass)
    {
        poisonous_grass.eaten(snake.index());
        if (snake->bodies().size() < 3)
            die(*snake);
        else
            snake->remove_tail();
    }
    bool can_eat(snake_object const &snake, landmine_object const &landmine)
    {
        for (auto const &body: snake.bodies())
            if (collides(body.outline(snake.outer_radius), landmine.outline()))
                return true;
        return false;
    }
    void eat(snake_iterator_t snake, landmine_object &landmine)
    {
        landmine.explode();
        if (snake->bodies().size() < 3)
            die(*snake);
        else
            cut_half(*snake);
    }
    void die(snake_object &snake)
    {
        snake.die();
        //TODO: add foods on snake bodies if multi-snakes is allowed.
    }
    void cut_half(snake_object &snake)
    {
        assert(snake.bodies().size() > 2);
        auto length = snake.bodies().size();
        for (auto i = 0u; i < length / 2; ++i)
            snake.remove_tail();
    }
    void check_hitting_the_wall()
    {
        check_snake_hitting_the_wall(*player_snake());
    }
    void check_snake_hitting_the_wall(snake_object &snake)
    {
        rect wall{point{0, 0}, map_size_};
        for (auto const &body:snake.bodies())
        {
            rect outline = body.outline(snake.outer_radius).surounding_rect();
            if (outline.top_left.x < wall.top_left.x || outline.top_left.y < wall.top_left.y ||
                outline.bottem_right().x > wall.bottem_right().x || outline.bottem_right().y > wall.bottem_right().y)
                hit_wall(snake);
        }
    }
    void hit_wall(snake_object &snake)
    {
        snake.die();
    }
    void check_mission()
    {
        if (win_condition())
            win();
        else
            check_to_enter_next_mission();
    }
    bool win_condition() const
    {
        return mission_ == 3 && score() >= 40;
    }
    void win()
    {
        pass_ = true;
    }
    void check_to_enter_next_mission()
    {
        if (mission_ == 1 && score() >= 20)
            mission_ = 2;
        else if (mission_ == 2 && score() >= 30)
            mission_ = 3;
        else
            return;
        generate_poisonous_grasses();
    }
    void update_step()
    {
        if (pass_ && win_displayed_step < max_win_displayed_step)
            ++win_displayed_step;
    }
    template <typename List>
    void update_objects(List &list)
    {
        for (auto iter = list.begin(); iter != list.end();)
        {
            iter->update();
            if (iter->need_to_be_removed())
                iter = list.remove(iter);
            else
                ++iter;
        }
    }

    unsigned int max_foods_number() const
    {
        return max_foods_number_;
    }
    unsigned int max_landmines_number() const
    {
        return mission_ * max_landmines_number_;
    }
    unsigned int max_poisonous_grasses_number() const
    {
        return mission_ * max_poisonous_grasses_number_;
    }
    double basic_speed() const
    {
        return 4.0 + (mission_ - 1) * 0.5;
    }
    foods_t foods_;
    landmines_t landmines_;
    poisonous_grasses_t poisonous_grasses_;
    snakes_t snakes;
    std::default_random_engine random_engine{(unsigned int) std::time(0)};
    size_type map_size_;
    static const unsigned int max_foods_number_ = 7;
    static const unsigned int max_landmines_number_ = 5;
    static const int max_poisonous_grasses_number_ = 15;
    bool is_game_over_ = false;
    unsigned int mission_ = 1;
    bool pass_ = false;
    int win_displayed_step = 0;
    static const int max_win_displayed_step = 60;
};
template <>
poisonous_grass_object snaker_gaming_model::create_object(point p)
{
    return {p, snakes};
}
template <>
food_object snaker_gaming_model::create_object(point p)
{
    return {p, random_color(), snakes};
}

#endif //SNAKER_SNAKER_MODEL_HPP
