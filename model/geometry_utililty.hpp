//
// Created by lizhe on 12/14/2016.
//

#ifndef SNAKER_GEOMETRY_UTILILTY_HPP
#define SNAKER_GEOMETRY_UTILILTY_HPP

#include <cmath>

class point;

inline point operator-(point lhs, point rhs);

class point
{
public:
    double x = 0, y = 0;

    double length() const
    {
        return std::sqrt(x * x + y * y);
    }
    double distance(point to) const
    {
        return (*this - to).length();
    }

    point& operator+=(point rhs)
    {
        x += rhs.x;
        y += rhs.y;
        return *this;
    }
    point operator-() const
    {
        return point{-x, -y};
    }
};
inline bool operator==(point lhs,point rhs)
{
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

inline point operator+(point lhs, point rhs)
{
    return point{lhs.x + rhs.x, lhs.y + rhs.y};
}
inline point operator-(point lhs, point rhs)
{
    return point{lhs.x - rhs.x, lhs.y - rhs.y};
}
template<typename T>
point operator*(point p, T scale)
{
    return point{p.x * scale, p.y * scale};
}
using direction = point;
using vector = point;
inline direction normalize(direction dir)
{
    return dir * (1.0 / dir.length());
}
inline double dot(direction lhs, direction rhs)
{
    return lhs.x * rhs.x + lhs.y * rhs.y;
}
inline double cross(direction lhs, direction rhs)
{
    return lhs.x * rhs.y - lhs.y * rhs.x;
}
inline direction orthogonal_direction(direction dir)
{
    return direction{-dir.y, dir.x};
}
inline direction average(direction lhs, direction rhs)
{
    return (lhs + rhs) * 0.5;
}
using size_type = point;

struct rect
{
    point top_left;
    size_type size;
    point center() const
    {
        return top_left + size * 0.5;
    }
    point top_right() const
    {
        return point{top_left.x + size.x, top_left.y};
    }
    point bottom_left() const
    {
        return point{top_left.x, top_left.y + size.y};
    }
    point bottem_right() const
    {
        return top_left + size;
    }

};

inline rect rect_from_center(point center, size_type s)
{
    return rect{center - s * 0.5, s};
}

struct circle
{
    point center;
    double radius;

    rect surounding_rect() const
    {
        return rect_from_center(center, size_type{radius, radius} * 2);
    }
    bool in_range(point p) const
    {
        return center.distance(p) < radius;
    }
    circle operator+(point p)
    {
        return circle{center + p, radius};
    }
    circle operator-(point p)
    {
        return *this + (-p);
    }
};

inline bool collides(circle c1, circle c2)
{
    return c1.center.distance(c2.center) < c1.radius + c2.radius;
}
inline bool collides(rect r1, rect r2)
{
    auto average_size = (r1.size + r2.size) * 0.5;
    return std::abs(r1.center().x - r2.center().x) < average_size.x &&
           std::abs(r1.center().y - r2.center().y) < average_size.y;
}
inline bool collides(circle c, rect r)
{
    if (collides(r, rect_from_center(c.center, size_type{c.radius * 2, c.radius * 2})))
    {
        if ((r.top_left.x - c.center.x) * (r.bottem_right().x - c.center.x) < 0 ||
            (r.top_left.y - c.center.y) * (r.bottem_right().y - c.center.y) < 0)
            return true;
        else
            return c.in_range(r.top_left) || c.in_range(r.top_right()) || c.in_range(r.bottem_right()) || c.in_range(r.bottom_left());
    }
    return false;
}
struct color_t
{
    unsigned char r = 0, g = 0, b = 0;
};
template<typename T>
color_t operator*(color_t c, T num)
{
    return color_t{(unsigned char)(c.r * num), (unsigned char)(c.g * num), (unsigned char)(c.b * num)};
}
inline color_t operator+(color_t lhs, color_t rhs)
{
    return color_t{(unsigned char)(lhs.r + rhs.r), (unsigned char)(lhs.g + rhs.g), (unsigned char)(lhs.b + rhs.b)};
}
inline double interpolate(double a, double b, double value)
{
    return a * value + b * (1 - value);
}
#endif //SNAKER_GEOMETRY_UTILILTY_HPP
