//
// Created by lizhe on 12/29/2016.
//

#ifndef SNAKER_GAME_PAGE_HPP
#define SNAKER_GAME_PAGE_HPP
namespace game_page
{
    enum class page
    {
        exit, home, gaming, rank, setting
    };

    class game_page
    {
    public:
        page current_page() const
        {
            return current_page_;
        }
        void go_to_parent_page()
        {
            switch (current_page_)
            {
            case page::gaming:
            case page::rank:
            case page::setting:
                current_page_ = page::home;
                break;
            case page::home:
            case page::exit:
                current_page_ = page::exit;
                break;
            }
        }
        void go_to(page p)
        {
            current_page_ = p;
        }
        bool is_home() const
        {
            return current_page_ == page::home;
        }
        bool is_gaming() const
        {
            return current_page_ == page::gaming;
        }
        bool is_rank() const
        {
            return current_page_ == page::rank;
        }
        bool is_setting() const
        {
            return current_page_ == page::setting;
        }
        bool is_exiting() const
        {
            return current_page_ == page::exit;
        }
    private:
        page current_page_ = page::home;
    };

}
#endif //SNAKER_GAME_PAGE_HPP
