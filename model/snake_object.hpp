//
// Created by lizhe on 12/14/2016.
//

#ifndef SNAKER_SNAKE_OBJECT_HPP
#define SNAKER_SNAKE_OBJECT_HPP

#include <cmath>
#include <iostream>
#include "geometry_utililty.hpp"
#include "../repository/list.hpp"

class snake_body
{
public:
    point position;
    circle outline(double radius) const
    {
        return {position,radius};
    }
};

class snake_object
{
    friend std::ostream& operator+(std::ostream& o, snake_object const& snake);
    friend std::istream& operator>>(std::istream& in, snake_object& snake);
public:
    using bodies_t = list<snake_body>;
    using iterator = bodies_t::iterator;
    using const_iterator = bodies_t::const_iterator;

    bool is_dead() const
    {
        return is_dead_;
    }
    void die()
    {
        is_dead_ = true;
    }
    bodies_t& bodies()
    {
        return bodies_;
    }
    bodies_t const& bodies() const
    {
        return bodies_;
    }
    snake_body& head()
    {
        return bodies().front();
    }
    snake_body const& head() const
    {
        return bodies().front();
    }
    void head_direction(direction dir)
    {
        head_direction_ = normalize(dir);
        limit_head_direction();
    }
    direction head_direction() const
    {
        return head_direction_;
    }
    direction neck_direction() const
    {
        return normalize(head().position - neck().position);
    }

    void add_at_tail()
    {
        add_body(bodies().back().position);
    }
    void add_at_tail(int number)
    {
        for (int i = 0; i < number; ++i)
        {
            add_at_tail();
        }
    }
    void add_body(point position)
    {
        bodies().insert(bodies().end(), snake_body{position});
    }
    void remove_tail()
    {
        bodies().remove(--bodies().end());
    }

    void move()
    {
        for (auto from = --bodies().end(); from != bodies().begin(); --from)
        {
            auto to = --iterator(from);
            auto direction = body_direction(from, to);
            auto move_speed = adjusted_speed(distance(from, to));
            from->position = from->position + direction * move_speed;

        }
        head().position = head().position + head_direction() * head_speed();
    }
    circle eating_range() const
    {
        return circle{head().position + neck_direction() * (eating_range_radius / 2), eating_range_radius};
    }
    double head_speed()
    {
        return basic_speed * accelerate_scale();
    }
    double accelerate_scale()
    {
        return accelerate_scale_;
    }
    double basic_speed = 4.0;
    double accelerate_scale_ = 1.0;
    static constexpr double outer_radius = 15;
    static constexpr double inner_radius = 1;
    color_t color_;
    color_t original_color_;
private:
    void limit_head_direction()
    {
        if (dot(head_direction(), neck_direction()) < sin60)
        {
            int inverse = cross(neck_direction(), head_direction()) > 0 ? 1 : -1;
            auto orthogonal_dir = orthogonal_direction(neck_direction()) * inverse * 0.5;
            head_direction_ = neck_direction() * sin60 + orthogonal_dir;
        }
    }
    snake_body const& neck() const
    {
        return *++bodies().begin();
    }
    double distance(const_iterator from, const_iterator to)
    {
        return from->position.distance(to->position);
    }
    direction body_direction(const_iterator from, const_iterator to)
    {
        auto dir = to->position - from->position;
        if (dir.length() < std::numeric_limits<double>::min())
            return direction{0, 0};
        else
            return normalize(dir);
    }
    double adjusted_speed(double distance)
    {
        return basic_speed * distance / body_distance;
    }
    bool is_dead_ = false;
    bodies_t bodies_;
    direction head_direction_;
    static constexpr double body_distance = 10;
    static constexpr double eating_range_radius = 16;
    static constexpr double sin60 = std::sqrt(3) / 2;
    static constexpr double sin45 = std::sqrt(2) / 2;
};

#endif //SNAKER_SNAKE_OBJECT_HPP
