//
// Created by lizhe on 12/25/2016.
//

#ifndef SNAKER_LANDMINE_OBJECT_HPP
#define SNAKER_LANDMINE_OBJECT_HPP

#include <istream>
#include <ostream>
#include "geometry_utililty.hpp"

class landmine_object
{
public:
    friend std::ostream &operator+(std::ostream &out, landmine_object const &landmine);
    friend std::istream &operator>>(std::istream &in, landmine_object &landmine);
    landmine_object() = default;
    explicit landmine_object(point pos)
        : position(pos)
    {}
    bool need_to_be_removed()
    {
        return is_exploded;
    }
    void explode()
    {
        is_exploded = true;
    }
    void update()
    {}
    rect outline() const
    {
        return rect_from_center(position, {2 * outer_radius, 2 * outer_radius});
    }

    point position;
    color_t color{0, 0, 0};
    static constexpr double inner_radius = 9;
    static constexpr double outer_radius = 12;
    static constexpr double size = outer_radius;
private:
    bool is_exploded = false;
};

#endif //SNAKER_LANDMINE_OBJECT_HPP
