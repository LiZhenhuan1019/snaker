//
// Created by lizhe on 12/29/2016.
//

#ifndef SNAKER_SNAKER_HONE_MODEL_HPP
#define SNAKER_SNAKER_HONE_MODEL_HPP
#include <string>
enum class button_on_home
{
    resume, play, rank, setting, exit
};

class snaker_home_model
{
public:
    button_on_home selected_button() const
    {
        return selected_button_;
    }
    bool selected(button_on_home button) const
    {
        return selected_button_ == button;
    }
    void select_up()
    {
        if (selected_button_ == button_on_home::resume)
            selected_button_ = button_on_home::exit;
        else
            selected_button_ = upward(selected_button_);
    }
    void select_down()
    {
        if (selected_button_ == button_on_home::exit)
            selected_button_ = button_on_home::resume;
        else
            selected_button_ = downward(selected_button_);
    }
    static button_on_home to_button(int i)
    {
        return static_cast<button_on_home>(i);
    }
    static constexpr int number_of_buttons()
    {
        return 5;
    }
    static std::string button_text(button_on_home button)
    {
        switch (button)
        {
        case button_on_home::resume:return "resume";
        case button_on_home::play:return "play";
        case button_on_home::rank:return "rank";
        case button_on_home::setting:return "setting";
        case button_on_home::exit:return "exit";
        }
        return "error button text";
    }
private:
    static button_on_home upward(button_on_home button)
    {
        return static_cast<button_on_home>(static_cast<int>(button) - 1);
    }
    static button_on_home downward(button_on_home button)
    {
        return static_cast<button_on_home>(static_cast<int>(button) + 1);
    }
    button_on_home selected_button_;
};

#endif //SNAKER_SNAKER_HONE_MODEL_HPP
