//
// Created by lizhe on 12/26/2016.
//

#ifndef SNAKER_POISONOUS_GRASS_HPP
#define SNAKER_POISONOUS_GRASS_HPP

#include <optional>
#include "../repository/indexed_repository.h"
#include "geometry_utililty.hpp"
#include "snake_object.hpp"

class poisonous_grass_object
{
    friend std::ostream &operator+(std::ostream &out, poisonous_grass_object const &grass);
    friend std::istream &operator>>(std::istream &in, poisonous_grass_object &grass);
public:
    using snake_repo_t = lzhlib::indexed_repository<snake_object>;
    using snake_index_t = snake_repo_t::index_t;
    poisonous_grass_object(point pos, snake_repo_t &snake_repo)
        : position(pos), snake_repo(&snake_repo)
    {}
    bool need_to_be_removed() const
    {
        return need_to_be_removed_;
    }
    bool hiden() const
    {
        return hiden_;
    }
    bool is_active() const
    {
        return is_active_;
    }
    void eaten(snake_index_t snake)
    {
        is_eaten_ = true;
        is_active_ = false;
        show_up();
        step = 0;
        eaten_by = snake;
    }
    void update()
    {
        if (!is_eaten_)
            blink();
        else
            eaten_effect();
        ++step;
    }
    rect outline() const
    {
        return rect_from_center(position, {size, size});
    }
    static constexpr double size = 10;
    point position;
    color_t color{128, 0, 255};
private:
    void show_up()
    {
        hiden_ = false;
    }
    void hide()
    {
        hiden_ = true;
    }
    void blink()
    {
        if (step == max_display_step)
            hide();
        else if (step == max_blink_step)
        {
            step = 0;
            show_up();
        }
    }
    void eaten_effect()
    {
        if (step == max_display_step_after_eaten)
            hide();
        if (step <= max_color_step)
            adjust_snake_color();
        else if (step == max_step_after_eaten)
            need_to_be_removed_ = true;
    }
    void adjust_snake_color() const
    {
        if (step == max_color_step)
            snake_eating().color_ = snake_eating().original_color_;
        else
        {
            double middle_step = max_color_step / 2;
            double value = std::abs(step - middle_step) / (middle_step);
            snake_eating().color_ = snake_eating().original_color_ * value + color * (1 - value);
        }
    }
    snake_object &snake_eating() const
    {
        return snake_repo->get_stock(eaten_by);
    }
    bool is_eaten_ = false;
    bool is_active_ = true;
    bool need_to_be_removed_ = false;
    bool hiden_ = false;
    int step = 0;
    static const int max_display_step = 10;
    static const int max_blink_step = 120;
    static const int max_display_step_after_eaten = 10;
    static const int max_color_step = 20;
    static const int max_step_after_eaten = 30;

    snake_index_t eaten_by = 0;
    snake_repo_t *snake_repo = nullptr;
};

#endif //SNAKER_POISONOUS_GRASS_HPP
