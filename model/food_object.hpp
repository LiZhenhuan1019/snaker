//
// Created by lizhe on 12/15/2016.
//

#ifndef SNAKER_FOOD_OBJECT_HPP
#define SNAKER_FOOD_OBJECT_HPP

#include <functional>
#include <iostream>
#include "geometry_utililty.hpp"
#include "../repository/indexed_repository.h"
#include "snake_object.hpp"

class food_object
{
    friend std::ostream &operator+(std::ostream &out, food_object const &food);
    friend std::istream &operator>>(std::istream &in, food_object &food);
public:
    using snake_repo_t = lzhlib::indexed_repository<snake_object>;
    using snake_index_t = snake_repo_t::index_t;
    food_object(point pos, color_t c, snake_repo_t &snake_repo)
        : position_(pos), food_color_(c), snake_repo(&snake_repo)
    {}
    bool need_to_be_removed() const
    {
        return need_to_be_removed_;
    }
    bool is_active() const
    {
        return is_active_;
    }
    void eaten(snake_index_t snake)
    {
        is_eaten = true;
        is_active_ = false;
        eaten_by = snake;
    }
    void update()
    {
        if (is_eaten)
        {
            ++step;
            if (step >= max_step())
                need_to_be_removed_ = true;
        }

    }
    circle outline() const
    {
        return circle{position(), outer_radius};
    }
    point position() const
    {
        if (is_eaten)
        {
            auto value = double(step) / pos_step;
            limit_interpolation(value);
            return position_ * (1 - value) + snake_eating().head().position * value;
        }
        else
            return position_;
    }
    color_t color() const
    {
        if (is_eaten)
        {
            auto value = double(step) / max_step();
            assert(value >= 0 && value <= 1);
            return food_color_ * (1 - value) + color_t{255, 255, 255} * value;
        }
        else
            return food_color_;
    }
    static constexpr double inner_radius = 2;
    static constexpr double outer_radius = 12;
    static constexpr double size = outer_radius;
private:
    static constexpr int max_step()
    {
        return pos_step + color_delay_step;
    }
    void limit_interpolation(double &value) const
    {
        if (value < 0)
            value = 0;
        else if (value > 1)
            value = 1;
    }
    snake_object const &snake_eating() const
    {
        return snake_repo->get_stock(eaten_by);
    }
    point position_;
    color_t food_color_;
    bool is_eaten = false;
    bool need_to_be_removed_ = false;
    bool is_active_ = true;
    int step = 0;
    static constexpr int pos_step = 10;
    static constexpr int color_delay_step = 10;
    snake_index_t eaten_by = 0;
    snake_repo_t *snake_repo = nullptr;
};

#endif //SNAKER_FOOD_OBJECT_HPP
