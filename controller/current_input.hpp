//
// Created by lizhe on 12/29/2016.
//

#ifndef SNAKER_CURRENT_INPUT_HPP
#define SNAKER_CURRENT_INPUT_HPP

#include "../model/geometry_utililty.hpp"
class current_input
{
public:
    bool left = false;
    bool right = false;
    bool up = false;
    bool down = false;
    bool accelerate = false;
    bool accelerated = false;
    point mouse_position;
};
#endif //SNAKER_CURRENT_INPUT_HPP
