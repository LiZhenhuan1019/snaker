//
// Created by lizhe on 12/29/2016.
//

#ifndef SNAKER_HOME_CONTROLLER_HPP
#define SNAKER_HOME_CONTROLLER_HPP

#include "../save/save_manager.hpp"
#include "../model/game_page.hpp"
#include "../model/snaker_home_model.hpp"
#include "current_input.hpp"
#include "gaming_controller.hpp"

class home_controller
{
public:
    home_controller(snaker_home_model& model,gaming_controller&gaming,game_page::game_page&page,current_input& input,save_manager& save)
        :model(model),gaming(gaming),page(page),input(input),save(save)
    {}
    void update()
    {
        update_home_model();
    }
private:
    void update_home_model()
    {
        update_selection();
        update_page();
    }
    void update_selection()
    {
        int go_down = input.right + input.down - input.up - input.left;
        if (pressed)
        {
            if (go_down == 0)
                pressed = false;
        }
        else
        {
            if (go_down != 0)
            {
                pressed = true;
                if (go_down > 0)
                    model.select_down();
                else
                    model.select_up();
            }
        }
    }
    void update_page()
    {
        if (input.accelerated)
        {
            if (!input.accelerate)
            {
                input.accelerated = false;
                enter_page();
            }
        }
        else
        {
            if (input.accelerate)
                input.accelerated = true;
        }
    }
    void enter_page()
    {
        switch (model.selected_button())
        {
        case button_on_home::resume:
            page.go_to(game_page::page::gaming);
            save.load();
            break;
        case button_on_home::play:
            page.go_to(game_page::page::gaming);
            gaming.restart();
            break;
        case button_on_home::rank:
            page.go_to(game_page::page::rank);
            break;
        case button_on_home::setting:
            page.go_to(game_page::page::setting);
            break;
        case button_on_home::exit:
            page.go_to(game_page::page::exit);
            break;
        }
    }
    snaker_home_model& model;
    gaming_controller&gaming;
    game_page::game_page&page;
    current_input& input;
    save_manager& save;
    bool pressed = false;
};
#endif //SNAKER_HOME_CONTROLLER_HPP
