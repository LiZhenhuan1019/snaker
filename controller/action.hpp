//
// Created by lizhe on 12/1/2016.
//

#ifndef SNAKER_ACTION_HPP
#define SNAKER_ACTION_HPP

#include <string>
#include <vector>

enum class action_type
{
    up, down, left, right, accelerate
};

class action
{
public:
    explicit action(std::string const& str);
    std::string const& name() const
    {
        return to_string(type_);
    }
    action_type type() const
    {
        return type_;
    }
private:
    static std::string const& to_string(action_type e)
    {
        return string_mapping[static_cast<std::size_t>(e)];
    }

    action_type type_;
    static std::vector<std::string> string_mapping;
};

#endif //SNAKER_ACTION_HPP
