//
// Created by lizhe on 11/30/2016.
//

#ifndef SNAKER_INPUT_MAPPING_HPP
#define SNAKER_INPUT_MAPPING_HPP

#include <map>
#include <string>
#include <optional>
#include "action.hpp"

using std::optional;
using std::nullopt;

class input_mapping
{
public:
    using key_type = int;
    void set(action action, key_type key)
    {
        mapping_.insert_or_assign(key, action);
    }
    std::optional<action> get_action(key_type key_value) const
    {
        auto iter = mapping_.find(key_value);
        if (iter != mapping_.end())
            return (*iter).second;
        else
            return nullopt;
    }
    std::map<key_type ,action> const& mapping() const
    {
        return mapping_;
    };
private:
    std::map<key_type, action> mapping_;
};

#endif //SNAKER_INPUT_MAPPING_HPP
