//
// Created by lizhe on 11/30/2016.
//

#ifndef SNAKER_CONTROLLER_HPP
#define SNAKER_CONTROLLER_HPP

#include <windows.h>
#include "action.hpp"
#include "../config/settings.hpp"
#include "../config/config_manager.hpp"
#include "../save/save_manager.hpp"
#include "../rank/rank_manager.hpp"
#include "../model/game_page.hpp"
#include "../model/snaker_gaming_model.hpp"
#include "../model/snaker_home_model.hpp"
#include "current_input.hpp"
#include "gaming_controller.hpp"
#include "home_controller.hpp"
#include "setting_controller.hpp"

class snaker_controller
{
public:
    snaker_controller(snaker_gaming_model& gaming_model, snaker_home_model& home_model, game_page::game_page& page,save_manager&save,config_manager& config,settings& setting,rank_manager& rank)
        : home_model(home_model),gaming_model(gaming_model), page(page),home(home_model,gaming,page,input,save),gaming(gaming_model,input,setting),setting(input,setting) ,save(save),config(config),set(setting),rank(rank)
    {}
    bool quit()
    {
        return will_quit;
    }
    bool process_key_down(HWND window, int key_value)
    {
        if (key_value == VK_ESCAPE)
        {
            if(page.is_gaming())
            {
                save.save();
                rank.update_rank(gaming_model.score());
            }
            else if(page.is_setting())
                config.write_config(set);
            page.go_to_parent_page();
        }
        else
            return false;
        return true;
    }
    bool process_key_up(HWND window, int key_value)
    {
        if (key_value == VK_ESCAPE)
        {

        }
        else
            return false;
        return true;
    }
    void action_down(action_type key)
    {
        switch (key)
        {
        case action_type::up:
            input.up = true;
            break;
        case action_type::down:
            input.down = true;
            break;
        case action_type::left:
            input.left = true;
            break;
        case action_type::right:
            input.right = true;
            break;
        case action_type::accelerate:
            input.accelerate = true;
            break;
        }
    }
    void action_up(action_type key)
    {
        switch (key)
        {
        case action_type::up:
            input.up = false;
            break;
        case action_type::down:
            input.down = false;
            break;
        case action_type::left:
            input.left = false;
            break;
        case action_type::right:
            input.right = false;
            break;
        case action_type::accelerate:
            input.accelerate = false;
            break;
        }
    }
    void mouse_move(point position)
    {
        input.mouse_position = position;
    }
    void update()
    {
        if (page.is_exiting())
            will_quit = true;
        else if (page.is_gaming())
            gaming.update();
        else if (page.is_home())
            home.update();
        else if(page.is_setting())
            setting.update();
    }

private:
    snaker_home_model& home_model;
    snaker_gaming_model& gaming_model;
    game_page::game_page& page;

    bool will_quit = false;
    current_input input;
    home_controller home;
    gaming_controller gaming;
    setting_controller setting;
    save_manager& save;
    config_manager& config;
    settings& set;
    rank_manager& rank;

};

#endif //SNAKER_CONTROLLER_HPP
