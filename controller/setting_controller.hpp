//
// Created by lizhe on 1/3/2017.
//

#ifndef SNAKER_SETTING_CONTROLLER_HPP
#define SNAKER_SETTING_CONTROLLER_HPP

#include "current_input.hpp"
#include "../config/settings.hpp"
class setting_controller
{
public:
    setting_controller(current_input& input,settings& setting)
        :input(input),setting(setting)
    {}
    void update()
    {
        update_setting();
    }
private:
    void update_setting()
    {
        update_controller_type();
    }
    void update_controller_type()
    {

        if (input.accelerated)
        {
            if (!input.accelerate)
            {
                input.accelerated = false;
                setting.set_use_mouse(!setting.use_mouse());
            }
        }
        else
        {
            if (input.accelerate)
                input.accelerated = true;
        }
    }

    current_input& input;
    settings& setting;
};
#endif //SNAKER_SETTING_CONTROLLER_HPP
