//
// Created by lizhe on 12/1/2016.
//
#include <algorithm>
#include "action.hpp"

action::action(std::string const& str)
{
    auto iter = std::find(string_mapping.begin(), string_mapping.end(), str);
    type_ = static_cast<action_type>(iter - string_mapping.begin());
}
std::vector<std::string> action::string_mapping =
    {
        "up", "down", "left", "right", "accelerate"
    };
