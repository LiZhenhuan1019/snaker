//
// Created by lizhe on 12/29/2016.
//

#ifndef SNAKER_GAMING_CONTROLLER_HPP
#define SNAKER_GAMING_CONTROLLER_HPP

#include "../model/game_page.hpp"
#include "../model/snaker_gaming_model.hpp"
#include "current_input.hpp"
#include "../config/settings.hpp"

class gaming_controller
{
public:
    gaming_controller(snaker_gaming_model& model, current_input& input, settings& setting)
        : model(model), input(input), setting(setting)
    {}
    void update()
    {
        update_gaming_model();
    }
    void restart()
    {
        model.reset();
    }
private:
    direction input_direction() const
    {
        if (setting.use_mouse())
            return mouse_direction();
        else
            return keyboard_direction();
    }
    direction mouse_direction() const
    {
        if (input.mouse_position == model.player_snake()->head().position)
            return model.player_snake()->neck_direction();
        else
            return normalize(input.mouse_position - model.player_snake()->head().position);
    }
    direction keyboard_direction() const
    {
        if (input.left - input.right || input.up - input.down)
            return direction{double(-input.left + input.right), double(-input.up + input.down)};
        else
            return model.player_snake()->neck_direction();

    }
    void update_gaming_model()
    {
        auto& player_snake = *model.player_snake();
        direction head_direction = input_direction();
        player_snake.head_direction(normalize(head_direction));
        if (input.accelerate && !input.accelerated)
        {
            if (model.is_game_over())
                restart();
            else
                player_snake.accelerate_scale_ = accelerate_scale;
            input.accelerated = true;
        }
        else if (!input.accelerate && input.accelerated)
        {
            player_snake.accelerate_scale_ = 1.0;
            input.accelerated = false;
        }
        model.update();
    }
    snaker_gaming_model& model;
    current_input& input;
    settings& setting;
    double accelerate_scale = 2.0;
};

#endif //SNAKER_GAMING_CONTROLLER_HPP
