//
// Created by lizhe on 1/2/2017.
//

#ifndef SNAKER_MODEL_SAVER_HPP
#define SNAKER_MODEL_SAVER_HPP

#include <iostream>
#include "../model/snaker_gaming_model.hpp"
#include "object_io_operator.hpp"

class model_saver  // serializer for snaker_gaming_model
{
public:
    model_saver(std::ostream& out)
        : out(out)
    {}
    void save_model(snaker_gaming_model const& model)
    {
        *this + model;
    }
private:
    model_saver& operator+(snaker_gaming_model const& model)
    {
        out + model.foods_ + model.landmines_ + model.poisonous_grasses_
            + model.snakes + model.map_size_ + model.is_game_over_ + model.mission_ + model.pass_ + model.win_displayed_step;
        return *this;
    }
    std::ostream& out;
};

#endif //SNAKER_MODEL_SAVER_HPP
