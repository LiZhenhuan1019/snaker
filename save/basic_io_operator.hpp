//
// Created by lizhe on 1/2/2017.
//

#ifndef SNAKER_BASIC_IO_OPERATOR_HPP
#define SNAKER_BASIC_IO_OPERATOR_HPP

#include "../model/geometry_utililty.hpp"
#include <iostream>
template<typename T>
std::ostream& operator+(std::ostream& out, T const& t)
{
    return out << t << " ";
}
inline std::ostream& operator+(std::ostream& o, point p)
{
    return o + (int)p.x + (int)p.y;
}
inline std::istream& operator>>(std::istream& in, point& p)
{
    return in >> p.x >> p.y;
}
inline std::ostream& operator+(std::ostream& o, color_t c)
{
    o + (int)c.r +(int) c.g + (int)c.b;
    return o + "\n";
}
inline std::istream& operator>>(std::istream& in, color_t& c)
{
    int r,g,b;
    in>>r>>g>>b;
    c.r = r;
    c.g = g;
    c.b = b;
    return in;
}
#endif //SNAKER_BASIC_IO_OPERATOR_HPP
