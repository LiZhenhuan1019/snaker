//
// Created by lizhe on 12/29/2016.
//

#ifndef SNAKER_SAVE_MANAGER_HPP
#define SNAKER_SAVE_MANAGER_HPP

#include <fstream>
#include <sstream>
#include <string>
#include "../model/snaker_gaming_model.hpp"
#include "model_saver.hpp"
#include "model_generator.hpp"
class save_manager
{
public:
    save_manager(snaker_gaming_model&model,std::string save_file_name = "save")
        :save_file_name(save_file_name),model(model)
    {}
    void save()
    {
        std::ofstream file(save_file_name);
        model_saver saver(file);
        std::string file_content;
        saver.save_model(model);
    }
    void load()
    {
        std::ifstream file(save_file_name);
        model_generator generator(file,model);
        generator.read_and_set_model();
    }
private:
    std::string save_file_name;
    snaker_gaming_model& model;
};
#endif //SNAKER_SAVE_MANAGER_HPP
