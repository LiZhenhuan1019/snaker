//
// Created by lizhe on 1/2/2017.
//

#ifndef SNAKER_REPOSITORY_IO_OPERATOR_HPP
#define SNAKER_REPOSITORY_IO_OPERATOR_HPP

#include <iostream>
#include "basic_io_operator.hpp"
#include "../repository/list.hpp"
#include "../repository/indexed_repository.h"

template<typename T>
std::ostream& operator+(std::ostream& out, list<T> const& list)
{
    out + list.size();
    for (auto const& t:list)
        out + t;
    return out;
}
template<typename T>
std::istream& operator>>(std::istream& in, list <T>& list)
{
    list.clear();
    int n = 0;
    in >> n;
    for (int i = 0; i < n; ++i)
    {
        T t;
        in >> t;
        list.insert(list.end(), t);
    }
    return in;
}
template<typename T>
std::ostream& operator+(std::ostream& out, lzhlib::indexed_repository<T> const& repo)
{
    out + repo.size();
    for (auto const& t : repo)
        out + t;
    return out;
}
template<typename T>
std::istream& operator>>(std::istream& in, lzhlib::indexed_repository<T>& repo)
{
    repo.clear();
    int n = 0;
    in >> n;
    for (int i = 0; i < n; ++i)
    {
        T t;
        in >> t;
        repo.set_stock(i, t);
    }
    return in;
}
#endif //SNAKER_REPOSITORY_IO_OPERATOR_HPP
