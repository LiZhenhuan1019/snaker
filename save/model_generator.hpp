//
// Created by lizhe on 1/2/2017.
//

#ifndef SNAKER_MODEL_GENERATOR_HPP
#define SNAKER_MODEL_GENERATOR_HPP

#include <iostream>
#include "basic_io_operator.hpp"
#include "object_io_operator.hpp"
#include "../repository/indexed_repository.h"
#include "../model/snaker_gaming_model.hpp"

class model_generator   // deserializer for snaker_gaming_model
{
public:
    using snake_repo_t = lzhlib::indexed_repository<snake_object>;
    model_generator(std::istream &in, snaker_gaming_model &model)
        : in(in), model(model)
    {}
    void read_and_set_model()
    {
        *this >> model;
    }
private:
    model_generator &operator>>(snaker_gaming_model &model)
    {
        return *this >> model.foods_ >> model.landmines_ >> model.poisonous_grasses_
                     >> model.snakes >> model.map_size_ >> model.is_game_over_ >> model.mission_ >> model.pass_ >> model.win_displayed_step;
    }
    template <typename T>
    model_generator &operator>>(T &t)
    {
        in >> t;
        return *this;
    }
    template <typename T>
    model_generator &operator>>(list<T> &list)
    {
        list.clear();
        int n = 0;
        *this >> n;
        for (int i = 0; i < n; ++i)
        {
            T t = create_object<T>();
            *this >> t;
            list.insert(list.end(), t);
        }
        return *this;
    }
    template <typename T>
    model_generator &operator>>(lzhlib::indexed_repository<T> &repo)
    {
        repo.clear();
        std::size_t n = 0;
        *this >> n;
        for (std::size_t i = 0; i < n; ++i)
        {
            T t = create_object<T>();
            *this >> t;
            repo.set_stock(i, std::move(t));
        }
        return *this;
    }
    template <typename T>
    T create_object()
    {
        return T{};
    }
    std::istream &in;
    snaker_gaming_model &model;
};
template <>
food_object model_generator::create_object<food_object>()
{
    return food_object{point{}, color_t{}, model.snakes};
}
template <>
poisonous_grass_object model_generator::create_object<poisonous_grass_object>()
{
    return poisonous_grass_object{point{}, model.snakes};
}

#endif //SNAKER_MODEL_GENERATOR_HPP
