//
// Created by lizhe on 1/1/2017.
//

#ifndef SNAKER_OBJECT_IO_OPERATOR_HPP
#define SNAKER_OBJECT_IO_OPERATOR_HPP

#include <iostream>
#include "basic_io_operator.hpp"
#include "repository_io_operator.hpp"
#include "../model/snake_object.hpp"
#include "../model/food_object.hpp"
#include "../model/landmine_object.hpp"
#include "../model/poisonous_grass_object.hpp"

inline std::ostream& operator+(std::ostream& o, snake_body const& body)
{
    return o + body.position;
}
inline std::istream& operator>>(std::istream& in, snake_body& body)
{
    return in >> body.position;
}

inline std::ostream& operator+(std::ostream& o, snake_object const& snake)
{
    return o + snake.accelerate_scale_ + snake.color_ + snake.original_color_ + snake.basic_speed + snake.is_dead_ + snake.bodies() + snake.head_direction_;
}
inline std::istream& operator>>(std::istream& in, snake_object& snake)
{
    return in >> snake.accelerate_scale_ >> snake.color_ >> snake.original_color_ >> snake.basic_speed >> snake.is_dead_ >> snake.bodies() >> snake.head_direction_;
}
inline std::ostream& operator+(std::ostream& out, food_object const& food)
{
    return out + food.position_ + food.food_color_ + food.is_eaten + food.need_to_be_removed() + food.is_active() + food.step + food.eaten_by;
}
inline std::istream& operator>>(std::istream& in, food_object& food)
{
    return in >> food.position_ >> food.food_color_ >> food.is_eaten >> food.need_to_be_removed_ >> food.is_active_ >> food.step >> food.eaten_by;
}
inline std::ostream& operator+(std::ostream& out, landmine_object const& landmine)
{
    return out + landmine.position + landmine.color + landmine.is_exploded;
}
inline std::istream& operator>>(std::istream& in, landmine_object& landmine)
{
    return in >> landmine.position >> landmine.color >> landmine.is_exploded;
}
inline std::ostream& operator+(std::ostream& out, poisonous_grass_object const& grass)
{
    return out + grass.position + grass.color + grass.is_eaten_ + grass.is_active_ + grass.need_to_be_removed_
    + grass.hiden_ + grass.step + grass.eaten_by;
}
inline std::istream& operator>>(std::istream& in, poisonous_grass_object& grass)
{
    return in >> grass.position >> grass.color >> grass.is_eaten_ >> grass.is_active_ >> grass.need_to_be_removed_
       >> grass.hiden_ >> grass.step >> grass.eaten_by;
}
#endif //SNAKER_OBJECT_IO_OPERATOR_HPP
