//
// Created by lizhe on 11/30/2016.
//
#include "test.hpp"
#include "../config/config_loader.hpp"
#include "../repository/list.hpp"
#include "../model/geometry_utililty.hpp"
#include <cassert>

void test_config_loader()
{
    settings setting;
    config_loader loader("snaker.json", setting);
    assert(setting.get_input_mapping().get_action(0x57).value().name() == "up");
    assert(setting.get_input_mapping().get_action(0x53).value().name() == "down");
    assert(setting.get_input_mapping().get_action(0x41).value().name() == "left");
    assert(setting.get_input_mapping().get_action(0x44).value().name() == "right");

}
namespace test_list
{
    void verify_empty(list<std::string> const& list)
    {
        assert(list.empty());
        assert(list.size() == 0);
        assert(list.begin() == list.end());
    }
    using iterator = list<std::string>::iterator;
    //    using const_iterator = list<std::string>::const_iterator;
    void verify_one_elements(iterator iter, list<std::string>& list)
    {
        assert(iter == list.begin());
        assert(!list.empty());
        assert(list.size() == 1);
        assert(++list.begin() == list.end());
        assert(list.begin() == --list.end());
        assert(*iter == "first");
        for (auto s : list)
            assert(s == "first");
    }
    void verify_two_elements(iterator iter1, iterator iter2, list<std::string>& list)
    {
        assert(!list.empty());
        assert(list.size() == 2);
        assert(iter1 == list.begin());
        assert(iter2 == ++list.begin() && --iterator(iter2) == list.begin() && iter2 == --list.end() && ++iterator(iter2) == list.end());
        assert(*iter1 == "first" && *iter2 == "second");
    }
    void verify_three_elements(iterator iter1, iterator iter2, iterator iter3, list<std::string>& list)
    {
        assert(!list.empty());
        assert(list.size() == 3);
        assert(iter1 == list.begin());
        assert(iter2 == ++list.begin() && --iterator(iter2) == list.begin() &&
               iter2 == --iterator(iter3) && ++iterator(iter2) == iter3 &&
               iter3 == --list.end() && ++iterator(iter3) == list.end());
    }
    void test_list()
    {
        list<std::string> list1;
        verify_empty(list1);

        {
            auto iter = list1.insert(list1.begin(), "first");
            verify_one_elements(iter, list1);
        }

        list1.clear();
        verify_empty(list1);

        {
            auto iter1 = list1.insert(list1.begin(), "first");
            auto iter2 = list1.insert(list1.end(), "second");
            verify_two_elements(iter1, iter2, list1);
        }
        list1.clear();
        verify_empty(list1);

        {
            auto iter3 = list1.insert(list1.begin(), "third");
            auto iter1 = list1.insert(list1.begin(), "first");
            auto iter2 = list1.insert(++list1.begin(), "second");
            verify_three_elements(iter1, iter2, iter3, list1);

            {
                auto end = list1.remove(iter3);
                assert(end == list1.end());
                verify_two_elements(iter1, iter2, list1);

                end = list1.remove(iter2);
                assert(end == list1.end());
                verify_one_elements(iter1, list1);

                end = list1.remove(iter1);
                assert(end == list1.end() && end == list1.begin());
                verify_empty(list1);
            }
        }
        list1.clear();
        verify_empty(list1);

        {
            auto iter2 = list1.insert(list1.begin(), "second");
            auto iter3 = list1.insert(list1.begin(), "third"); // will be removed
            auto iter1 = list1.insert(list1.begin(), "first");

            list1.remove(iter3);
            verify_two_elements(iter1, iter2, list1);
        }
    }
}

namespace test_collides
{
    void test_rects()
    {
        rect r1{{0,  0},
                {10, 10}};
        rect r2{{8, 8},
                {5,  5}};
        assert(collides(r1,r2));

        rect r3{{8,5},{5,3}};
        assert(collides(r1,r3));

        rect r4{{5,8},{3,5}};
        assert(collides(r1,r4));

        rect r5 {{11,11},{3,3}};
        assert(!collides(r1,r5));
    }
    void test_circle_and_rect()
    {
        circle c{{0,0},10};
        rect r1{15,15,5,5};
        assert(!collides(c,r1));

        rect r2{-5,9,10,10};
        assert(collides(c,r2));

        rect r3{7,7,5,5};
        assert(collides(c,r3));

        rect r4{-5,11,10,10};
        assert(!collides(c,r4));
    }
    void test_collides()
    {
        test_rects();
        test_circle_and_rect();
    }
}
void test()
{
    test_config_loader();
    test_list::test_list();
    test_collides::test_collides();
}

